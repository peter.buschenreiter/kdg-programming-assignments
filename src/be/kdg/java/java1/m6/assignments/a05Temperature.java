package be.kdg.java.java1.m6.assignments;

import java.util.Scanner;

/**
 * Write a program in which you ...
 * <p>
 * - ask the user for 7 temperatures. Store them in an array. <br>
 * - display these temperatures in a nicely aligned table. Check the formatting! <br>
 * - calculate the average temperature and display it. <br>
 * - Use a constant NUMBER_OF_TEMPERATURES with a value of 7. <br>
 */

public class a05Temperature {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final int NUMBER_OF_TEMPERATURES = 3;
        float[] temperatures = new float[NUMBER_OF_TEMPERATURES];
        int sum = 0;
        final String SEPARATOR = "=================";

        System.out.printf("Please enter %d temperatures: %n", NUMBER_OF_TEMPERATURES);
        for (int i = 0; i < NUMBER_OF_TEMPERATURES; i++) {
            System.out.printf("Day %d: ", i + 1);
            temperatures[i] = keyboard.nextFloat();
        }
        keyboard.close();

        for (float temp : temperatures) {
            sum += temp;
        }

        System.out.println("Summary:");
        System.out.println(SEPARATOR);
        for (int i = 0; i < NUMBER_OF_TEMPERATURES; i++) {
            System.out.printf("Day %d: %10.1f %n", i + 1, temperatures[i]);
        }
        System.out.println(SEPARATOR);

        System.out.printf("Average: %.2f %n", (float) sum / NUMBER_OF_TEMPERATURES);
    }
}
