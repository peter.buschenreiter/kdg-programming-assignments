package be.kdg.java.java1.m6.assignments;

import java.util.Scanner;

/**
 * Write a program that asks the user for a sentence and then prints how many times each letter appears in this sentence.
 *<p>
 * - Create an array that holds 26 integers (the amount of times each letter appears in the sentence).<br>
 * - Let's imagine that position '0' holds the amount of times an 'a' appears, position '1' holds the amount of times an 'b' appears, and so on...<br>
 * - All 26 integers should be initialized to zero. Do you have to write additional code to do this? Why (not)? <br>
 * - Ask the user for a sentence and convert that sentence to lower case.<br>
 * - Iterate through each individual character of the sentence using a loop. You can use charAt to get one single letter from the String.<br>
 * - Using an ASCII table, try to determine whether the character is actually a letter.<br>
 * - Also using the ASCII table, use the decimal value of the character in question to calculate which index in the array needs an update.<br>
 * - Now, increment the integer at that index location.<br>
 * - While iterating, also keep track of the total amount of letters.<br>
 * - Next, print the frequencies as in the sample output below. You'll need the ASCII table one more time.<br>
 * - Make sure to output a newline every four letters.<br>
 * - Finally, display the total amount of found letters.<br>
 */

public class a06CountingLetters {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String sentence;
        int letterCount = 0;
        int[] amountOfLetters = new int[26]; // not necessary to write additional code to initialize the ints to 0 --> is being done by default

        System.out.print("Please enter a sentence: ");
        sentence = keyboard.nextLine();
        keyboard.close();
        char[] letters = sentence.toLowerCase().toCharArray();
        System.out.println("Letter frequencies: ");

        for (char letter : letters) {
            if (letter < 97 || letter > 97 + 26) continue;
            amountOfLetters[letter - 97]++;
            letterCount++;
        }

        for (int i = 0; i < amountOfLetters.length; i++) {
            System.out.printf("%c --> %d times \t", (char) (i + 97), amountOfLetters[i]);
            if ((i + 1) % 4 == 0) System.out.println();
        }
        System.out.println();

        System.out.printf("Total amount of letters: %d %n", letterCount);
    }
}
