package be.kdg.java.java1.m6.assignments;

/**
 * - Declare an array of four StringBuilder objects with the name suits. Don't use the array literal syntax.
 *<br>
 * - Give the elements the values "hearts", "clubs", "diamonds" and "spades".
 *<br>
 * - Print the contents of this array using a for-each loop.
 *<br>
 * - Repeat the assignment, but this time make sure you initialize the contents of the array (the four elements) at the time of declaration.
 */

public class a07ArraysV2 {
    public static void main(String[] args) {
        // NOT array literal syntax
        StringBuilder[] suits = new StringBuilder[4];
         suits[0] = new StringBuilder("hearts");
         suits[1] = new StringBuilder("clubs");
         suits[2] = new StringBuilder("diamonds");
         suits[3] = new StringBuilder("spades");

        for (StringBuilder suit : suits) {
            System.out.println(suit);
        }

        // array literal syntax
        StringBuilder[] suitsLiteral = {
                new StringBuilder("hearts"),
                new StringBuilder("clubs"),
                new StringBuilder("diamonds"),
                new StringBuilder("spades")
        };

        for (StringBuilder suit : suitsLiteral) {
            System.out.println(suit);
        }
    }
}
