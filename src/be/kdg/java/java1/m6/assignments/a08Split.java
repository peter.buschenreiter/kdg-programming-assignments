package be.kdg.java.java1.m6.assignments;

/**
 * Create an array of String objects by using the split method on text with a single space as regular expression (" ").
 *<br>
 * Finally, use a loop to print each element of the String array, separated by a space:<br>
 * The double quotes around each word must be displayed in the output!
 */

public class a08Split {
    public static void main(String[] args) {
        String text = "Java can be tricky at times";
        String[] splitText = text.split(" ");

        for (String word : splitText) {
            System.out.printf("\"%s\" ", word);
        }
    }
}
