package be.kdg.java.java1.m6.assignments;

import java.util.Random;

/**
 * Declare the following (empty) arrays:
 *<p>
 * - an array called numbers for 5 integers <br>
 * - an array called stockMarketRates for 20 floating point numbers <br>
 * - an array called switches for 8 booleans <br>
 * - an array called words for 4 strings <br>
 * - Then print the first element of each array. Finally, also print the last element of each array. <br>
 */

public class a01ArraysV1 {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        float[] stockMarketRates = new float[20];
        boolean[] switches = new boolean[8];
        String[] words = new String[4];
        Random rand = new Random();

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = rand.nextInt();
        }
        for (int i = 0; i < stockMarketRates.length; i++) {
            stockMarketRates[i] = rand.nextFloat();
        }
        for (int i = 0; i < switches.length; i++) {
            switches[i] = rand.nextBoolean();
        }
        for (int i = 0; i < words.length; i++) {
            words[i] = "" + (char) (rand.nextInt(26) + 65) + (char) (rand.nextInt(26) + 65);
        }
        System.out.printf("Numbers: First: %d Last: %d %n", numbers[0], numbers[numbers.length - 1]);
        System.out.printf("StockMarketRates: First: %f Last: %f %n", stockMarketRates[0], stockMarketRates[stockMarketRates.length - 1]);
        System.out.printf("Switches: First: %b Last: %b %n", switches[0], switches[switches.length - 1]);
        System.out.printf("Words: First: %s Second: %s", words[0], words[words.length - 1]);
    }
}
