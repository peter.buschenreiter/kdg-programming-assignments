package be.kdg.java.java1.m6.assignments.a10playingcards;

/**
 * Create a Card class with two text attributes: rank and suit.<br>
 * Create a constructor with two parameters in which you initialize both attributes.<br>
 * Make sure that the attributes can't be accessed from outside the class.<br>
 * Create getters for both attributes.
 */

public class Card {
    private String rank;
    private String suit;

    // CONSTRUCTORS
    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    // GETTERS
    public String getRank() {
        return rank;
    }

    public String getSuit() {
        return suit;
    }
}
