package be.kdg.java.java1.m6.assignments;

/**
 * Create a matrix of integers with four rows and six columns.<br>
 *
 * Using two nested loops, fill the matrix by assigning each element a value equal to the product of the row number and the column number.<br>
 *
 * Print the matrix row by row.
 */

public class a11Matrix {
    public static void main(String[] args) {
        int[][] matrix = new int[4][6];

        for (int col = 0; col < matrix.length; col++) {
            for (int row = 0; row < matrix[col].length; row++) {
                System.out.printf("%4d ", (col +1) * (row + 1));
            }
            System.out.println();
        }
    }
}
