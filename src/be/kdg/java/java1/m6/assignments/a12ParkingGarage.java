package be.kdg.java.java1.m6.assignments;

import java.util.Random;

/**
 * Write a program to implement a tracking system for parking garages.
 * <p>
 * - Create a three-dimensional array called parkingSpots with which we can keep track of the amount of times a certain parking spot was occupied.<br>
 * - The first dimension represents the garages. Make sure there are two garages.<br>
 * - The second dimension represents the amount of floors per garage. Make sure there are two floors per garage.<br>
 * - The third dimension represents a parking spot (or parking location) and will contain the amount of times that it was taken. Each floor has 15 parking spots.<br>
 * - Initialize all-  the parking spots with a number from 0 to 9 (both inclusive).<br>
 * - You'll have to use three nested loops (each loop iterating over a certain dimension).<br>
 * - Finally, calculate the total amount of times that each garage was used by summing the amount of times that each of its parking spots was used (across all floors).<br>
 * - You'll need nested loops once again to sum this total per garage.<br>
 */

public class a12ParkingGarage {
    public static void main(String[] args) {
        final int AMOUNT_GARAGES = 2;
        int[][][] parkingSpots = new int[AMOUNT_GARAGES][2][15];
        int[] spotsUsedPerGarage = new int[AMOUNT_GARAGES];

        Random rand = new Random();

        for (int garage = 0; garage < parkingSpots.length; garage++) {
            for (int floor = 0; floor < parkingSpots[garage].length; floor++) {
                for (int spot = 0; spot < parkingSpots[garage][floor].length; spot++) {
                    parkingSpots[garage][floor][spot] = rand.nextInt(10);
                }
            }
        }

        for (int garage = 0; garage < parkingSpots.length; garage++) {
            for (int floor = 0; floor < parkingSpots[garage].length; floor++) {
                for (int spot = 0; spot < parkingSpots[garage][floor].length; spot++) {
                    spotsUsedPerGarage[garage] += parkingSpots[garage][floor][spot];
                }
            }
            System.out.printf("Garage %d: used %d times %n", garage + 1, spotsUsedPerGarage[garage]);
        }
    }
}
