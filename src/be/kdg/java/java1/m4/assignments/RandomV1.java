package be.kdg.java.java1.m4.assignments;

import java.util.Random;

/**
 * Create an object of type Random. Use it to produce the following output:
 *<p>
 * Print six random numbers from 1 to 6 (both inclusive) on a single line<br>
 * Print four random booleans on a single line<br>
 * Print three random decimals from 0 to 1 (not including 1)<br>
 * Additional challenge: also try to print the following output using the same Random object:<br>
 *<p>
 * 10 random numbers between 900 and 1000 (both inclusive)<br>
 * 10 random decimals between 5 and 10 (i.e. 6.481)<br>
 * 10 random even numbers between 0 and 100<br>
 * 10 random multiples of 3 smaller than 100<br>
 */

public class RandomV1 {
    public static void main(String[] args) {
        Random random = new Random();
        // 6 random ints between 1 and 6
        System.out.println("Ints from 1 to 6");
        for (int i = 0; i < 6; i++) {
            System.out.printf("%d ", random.nextInt(1,7));
        }
        System.out.println();
        // 4 random booleans
        System.out.println("Booleans");
        for (int i = 0; i < 4; i++) {
            System.out.printf("%S ", random.nextBoolean());
        }
        System.out.println();
        // 3 random doubles between 0 and 1
        System.out.println("Doubles between 0 and 1");
        for (int i = 0; i < 3; i++) {
            System.out.printf("%f ", random.nextDouble());
        }
        System.out.println();
        // 10 random ints from 900 to 1000
        System.out.println("Ints from 900 to 1000");
        for (int i = 0; i < 10; i++) {
            System.out.printf("%d ", random.nextInt(900, 1001));
        }
        System.out.println();
        // 10 random doubles between 5 and 10
        System.out.println("Doubles between 5 and 10");
        for (int i = 0; i < 10; i++) {
            System.out.printf("%f ", random.nextDouble(5, 11));
        }
        System.out.println();
        // 10 random even numbers from 0 to 100 #1
        System.out.println("Even numbers from 0 to 100");
        for (int i = 0; i < 10; i++) {
            final int BOUND = 100;
            int randomNum = random.nextInt(BOUND);
            if (randomNum % 2 == 1) {
                if (randomNum < BOUND / 2) randomNum++;
                else randomNum--;
            }
            System.out.printf("%d ", randomNum);
        }
        System.out.println();
        // 10 random even numbers from 0 to 100 #2
        for (int i = 0; i < 10; i++) {
            System.out.printf("%d ", random.nextInt(50) * 2);
        }
        System.out.println();
        // 10 random multiples of 3 under 100 #1
        System.out.println("Multiples of 3 under 100");
        for (int i = 0; i < 10; i++) {
            final int BOUND = 100;
            int randomNum = random.nextInt(BOUND);
            int remainder = randomNum % 3;
            if (remainder != 0) {
                if (randomNum < BOUND / 2) randomNum += (3 - remainder);
                else randomNum -= remainder;
            }
            System.out.printf("%d ", randomNum);
        }
        System.out.println();
        // 10 random multiples of 3 under 100 #2
        for (int i = 0; i < 10; i++) {
            System.out.printf("%d ", random.nextInt(100 / 3) * 3);
        }
    }
}
