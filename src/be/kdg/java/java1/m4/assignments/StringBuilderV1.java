package be.kdg.java.java1.m4.assignments;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Let the user enter his or her first name and last name, separated by a space.
 *<p>
 * Create four StringBuilder objects:
 *<p>
 * builderOne: should only contain the initials<br>
 * builderTwo: should contain the reversed name<br>
 * builderThree: should contain the name with each 'e' replaced by an 'a' (don't use String's "replace" or "replaceAll" methods!)<br>
 * builderFour: should contain all the characters in a random order<br>
 * Try to use StringBuilder's methods as much as possible.<br>
 */

public class StringBuilderV1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        ArrayList<Integer> usedIndexes = new ArrayList<>();

        System.out.print("Enter your name: ");
        String name = keyboard.nextLine();

        StringBuilder builderInitials = new StringBuilder(name);
        StringBuilder builderReversed = new StringBuilder(name);
        StringBuilder builderReplacer = new StringBuilder(name);
        StringBuilder builderRandom = new StringBuilder();

        // initials
        builderInitials.delete(1, builderInitials.indexOf(" ") + 1).delete(2, builderInitials.length());
        System.out.println("Initials: " + builderInitials);

        // reverse
        builderReversed.reverse();
        System.out.println("Reversed: " + builderReversed);

        // replace e with a
        int idx;
        while ((idx = builderReplacer.indexOf("e")) >= 0) {
            builderReplacer.replace(idx,idx + 1, "a");
        }
        while ((idx = builderReplacer.indexOf("E")) >= 0) {
            builderReplacer.replace(idx,idx + 1, "A");
        }
        System.out.println("Replaced: " + builderReplacer);

        // scramble the letters
        for (int i = 0; i < name.length(); i++) {
            int index = (int) Math.floor(Math.random()*name.length()); // calculate random index
            while (usedIndexes.contains(index)) { // if index has already been used, go to next index
                index = (index + 1) % name.length(); // % operation to stay within bounds
            }
            usedIndexes.add(index);
            builderRandom.append(name.charAt(index));
        }
        System.out.println("Random: " + builderRandom);

        keyboard.close();
    }
}
