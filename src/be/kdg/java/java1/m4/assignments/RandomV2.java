package be.kdg.java.java1.m4.assignments;

import java.util.Random;

/**
 * Create an object of type Random and use it to print five random decimals on a single line. Make sure they're printed with just two decimal digits. (Hint: System.out.printf)
 *<p>
 * Next, create two new objects of type Random and initialize both of them with a seed of '42'. Generate 10 random numbers between 1 and 42 (both inclusive) using both objects, so 20 numbers in total. Print the random numbers while alternating between the two Random objects.
 *<p>
 * Try to explain the second line of the output.
 */

public class RandomV2 {
    public static void main(String[] args) {
        Random random = new Random();
        Random random42 = new Random(42);
        Random random42_2 = new Random(42);
        for (int i = 0; i < 5; i++) {
            System.out.printf("%.2f ", random.nextDouble());
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.printf("%2d --- %2d --- %2d",random.nextInt(1, 42), random42.nextInt(1, 43), random42_2.nextInt(1, 43));
            System.out.println();
            // both columns of numbers will be the same, because they use the same seed
        }
    }
}
