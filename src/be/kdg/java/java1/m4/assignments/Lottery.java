package be.kdg.java.java1.m4.assignments;

import java.util.Random;

/**
 *
 * 1. Declare and initialise an object of type Random. Name this object "random". Call the constructor that doesn't take any parameters.<br><p>
 * 2. Print ten random lottery numbers in range [1..45] (both inclusive)
 * Use a for-loop to do this.<br><p>
 * 3. Run your program a couple of times. Does it print the same numbers every time or different numbers?<br><p>
 * 4. Change your code in such a way that the other constructor of class Random is called instead... the one that takes a single parameter.<br>
 * You can use, for example, "100" (without quotes) as a value for this parameter.<br><p>
 * 5. Check the output again and compare the output of subsequent executions of your program. Are you seeing the same numbers or different numbers?
 */

public class Lottery {
    public static void main(String[] args) {
        Random random = new Random();
        Random random100 = new Random(100);
        System.out.println("random --- random100");
        for (int i = 0; i < 10; i++) {
            System.out.printf("%6d --- %2d\n",random.nextInt(1,45), random100.nextInt(1, 45));
        }
    }
}
