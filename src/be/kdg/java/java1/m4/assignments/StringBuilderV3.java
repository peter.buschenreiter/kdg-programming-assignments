package be.kdg.java.java1.m4.assignments;

import java.util.Scanner;

/**
 * Complete this program so that the expected output is printed.
 * 
 * Expected output:
 * 
 * Enter word 1: apple
 * Enter word 2: pear
 * Enter word 3: banana
 * Enter word 4: lemon
 * Enter word 5: Java
 * 
 * Content of builder: apple pear banana lemon Java 
 * Content of copy: apple pear banana lemon Java
 * 
 * Comparison with == results in: false
 * Comparison with equals results in: false
 * 
 * Upper case: APPLE PEAR BANANA LEMON JAVA
 * 
 */

public class StringBuilderV3 {
    public static void main(String[] args) {

    // beginning of copied code
        final int AMOUNT = 5;
        Scanner keyboard = new Scanner(System.in);
        StringBuilder builder = new StringBuilder();

        // Read AMOUNT words and append each word to the StringBuilder.
        // Use a for-loop and 'printf'.

        // Print the content of 'builder'.

        // Create a copy of the StringBuilder object and name it 'copy'. Make sure it contains
        // the same content as the original StringBuilder.
        // Print the content of 'copy'.

        // Now check if 'builder' has the same content as 'copy'. Try '==' as well as the 'equals' method.
        // Note: unlike 'String', 'StringBuilder' doesn't actually have an implementation of the 'equals'
        // method. Yet, we can still call the 'equals' method on objects of type StringBuilder.
        // This will be explained later on! (You might want to take a look at the 'Object' class.)

        // Convert builder to upper case without using the String class and without creating
        // a new StringBuilder.
        // Hint: use an ASCII table.  
    // end of copied code

        for (int i = 0; i < AMOUNT; i++) {
            System.out.printf("Enter word %d: ", i + 1);
            builder.append(keyboard.next()).append(' ');
        }
        keyboard.close();

        System.out.println();
        System.out.printf("Content of builder: %s %n", builder);
        StringBuilder copy = new StringBuilder(builder);
        System.out.printf("Content of copy: %s %n", copy);
        System.out.println();
        System.out.printf("Comparison with == results in: %b %n", builder == copy);
        System.out.printf("Comparison with equals results in: %b %n", builder.equals(copy));
        System.out.println();

        for (int i = 0; i < builder.length(); i++) {
            if (builder.charAt(i) == ' ' || (int) builder.charAt(i) < 91) continue;
            builder.setCharAt(i, (char) ((builder.charAt(i) - 71) % 26 + 65));
        }
        System.out.printf("Upper case: %s %n", builder);
    }
}
