package be.kdg.java.java1.m4.assignments;

/**
 * Execute the following code. Simply add it to a main method and check the output:
 */

public class StringTests {
    public static void main(String[] args) {
    // copied code from assignment
        String one = "abc";
        String two = "abc";
        String three = new String("abc");

        System.out.println(one == two);
        System.out.println(one == three);
        System.out.println(two == three);

        System.out.println(one.equals(two));
        System.out.println(one.equals(three));
        System.out.println(two.equals(three));
    // end of copied code

        // Strings should only be compared using .equals(), because it compares by value, == compares by reference
    }
}
