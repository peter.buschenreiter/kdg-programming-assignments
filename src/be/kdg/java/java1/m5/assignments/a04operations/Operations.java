package be.kdg.java.java1.m5.assignments.a04operations;

public class Operations {
    private int numberOne;
    private int numberTwo;

    public Operations(int numberOne, int numberTwo) {
        setNumberOne(numberOne);
        setNumberTwo(numberTwo);
    }

    //GETTERS & SETTERS
    public int getNumberOne() {
        return numberOne;
    }

    public void setNumberOne(int numberOne) {
        this.numberOne = numberOne;
    }

    public int getNumberTwo() {
        return numberTwo;
    }

    public void setNumberTwo(int numberTwo) {
        this.numberTwo = numberTwo;
    }

    //METHODS
    public int sum() {
        return getNumberOne() + getNumberTwo();
    }

    public int difference() {
        return Math.max(getNumberOne(), getNumberTwo()) - Math.min(getNumberOne(), getNumberTwo());
    }

    public long product() {
        return (long) getNumberOne() * getNumberTwo();
    }

    public double division() {
        return (double) getNumberOne() / getNumberTwo();
    }

    @Override
    public String toString() {
        return String.format(
                "The sum is %d %n" +
                "The difference is %d %n" +
                "The product is is %d %n" +
                "The quotient is %f", sum(), difference(), product(), division());
    }
}
