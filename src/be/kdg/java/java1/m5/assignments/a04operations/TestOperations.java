package be.kdg.java.java1.m5.assignments.a04operations;

import be.kdg.java.java1.m3_flow_control.assignments.HelpMe;

public class TestOperations {
    public static void main(String[] args) {
        final int MIN = 0;
        final int MAX = 100_000_000;
        int num1;
        int num2;
        num1 = HelpMe.readNumberBetweenBounds(MIN, MAX, "integer");
        System.out.println();
        num2 = HelpMe.readNumberBetweenBounds(MIN, MAX, "integer");

        Operations operations = new Operations(num1, num2);
        System.out.println(operations);
    }
}
