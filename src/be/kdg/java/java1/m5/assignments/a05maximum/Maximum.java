package be.kdg.java.java1.m5.assignments.a05maximum;

public class Maximum {
    private int one;
    private int two;
    private int three;

    // CONSTRUCTORS
    public Maximum() {
        System.out.println("Constructor without parameters called");
    }

    public Maximum(int one, int two, int three) {
        setOne(one);
        setTwo(two);
        setThree(three);
        System.out.printf("%s constructor called %n", ((Object)one).getClass().getSimpleName());
    }

    public Maximum(long one, long two, long three) {
        setOne((int) one);
        setTwo((int) two);
        setThree((int) three);
        System.out.printf("%s constructor called %n", ((Object)one).getClass().getSimpleName());
    }

    public Maximum(double one, double two, double three) {
        setOne((int) one);
        setTwo((int) two);
        setThree((int) three);
        System.out.printf("%s constructor called %n", ((Object)one).getClass().getSimpleName());
    }

    // GETTERS & SETTERS
    public int getOne() {
        return one;
    }

    public void setOne(int one) {
        this.one = one;
    }

    public int getTwo() {
        return two;
    }

    public void setTwo(int two) {
        this.two = two;
    }

    public int getThree() {
        return three;
    }

    public void setThree(int three) {
        this.three = three;
    }

    //METHODS
    public double max() {
        return Math.max(Math.max(one, two), three);
    }

    public double max(int one, int two, int three) {
        System.out.printf("%s parameters method called %n", ((Object)one).getClass().getSimpleName());
        return Math.max(Math.max(one, two), three);
    }

    public double max(long one, long two, long three) {
        System.out.printf("%s parameters method called %n", ((Object)one).getClass().getSimpleName());
        return Math.max(Math.max(one, two), three);
    }

    public double max(double one, double two, double three) {
        System.out.printf("%s parameters method called %n", ((Object)one).getClass().getSimpleName());
        return Math.max(Math.max(one, two), three);
    }
}
