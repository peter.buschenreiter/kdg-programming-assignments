package be.kdg.java.java1.m5.assignments.a01book;

public class Book {
    private String author;
    private String title;
    private int pages;
    private boolean onLoan;

    //CONSTRUCTORS
    public Book() {
        this("unknown", "unknown", 0);
    }

    public Book(String author, String title, int pages){
        setAuthor(author);
        setTitle(title);
        setPages(pages);
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public boolean isOnLoan() {
        return onLoan;
    }

    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }

    //METHODS
    @Override
    public String toString() {
        return String.format("Book %s (%d), written by %s is %son loan. %n",
                title.toUpperCase(),
                pages,
                author.toUpperCase(),
                isOnLoan() ? "" : "not ");
    }
}
