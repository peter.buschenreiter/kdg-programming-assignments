package be.kdg.java.java1.m5.assignments.a01book;

public class TestBook {
    public static void main(String[] args) {
        Book book1 = new Book();
        Book book2 = new Book("JK Rowling", "Harry Potter", 456);
        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
