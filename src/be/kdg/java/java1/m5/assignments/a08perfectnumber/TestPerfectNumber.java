package be.kdg.java.java1.m5.assignments.a08perfectnumber;

public class TestPerfectNumber {
    public static void main(String[] args) {
        PerfectNumber perfectNumber = new PerfectNumber();
        for (long i = 2; i <= 1_000_000_000L; i++) {
            if (perfectNumber.getPerfect(i) != null) {
                System.out.println(perfectNumber.getPerfect(i));
            }
        }
    }
}
