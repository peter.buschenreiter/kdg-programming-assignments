package be.kdg.java.java1.m5.assignments.a06salesperson;

public class SalesPerson {
    private String name;
    private double revenue;

    //CONSTRUCTORS
    public SalesPerson(String name, double revenue) {
        setName(name);
        setRevenue(revenue);
    }

    // GETTERS & SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    // METHODS
    public SalesPerson hasMoreRevenueThan(SalesPerson other) {
        if (getRevenue() > other.getRevenue()) {
            return this;
        } else {
            return other;
        }
    }
}
