package be.kdg.java.java1.m5.assignments.a02bankaccount;

public class BankAccount {
    private String holder;
    private String iban;
    private double balance;

    public BankAccount(String holder, String iban) {
        this(holder, iban, 0);
    }

    public BankAccount(String holder, String iban, double balance) {
        setHolder(holder);
        setIban(iban);
        setBalance(balance);
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    //METHODS
    public void deposit(double amount) {
        if (amount > 0) {
            setBalance(balance + amount);
        } else {
            System.out.println("Invalid amount");
        }
    }
    public boolean withdraw(double amount) {
        if (balance - amount < 0) {
            return false;
        } else {
            setBalance(balance - amount);
            return true;
        }
    }
    @Override
    public String toString() {
        return String.format("The account %s of %s has a balance of € %.2f", iban, holder, balance);
    }
}
