package be.kdg.java.java1.m3_flow_control.assignments;

/*
  Ask a user for the number of a month.

  Print the name of the month and the number of days.

  There should be only one println statement in your program for printing the result.

  Extend your program to also ask the user for a year.

  If the year is a leap year, february should have 29 days.

  A year is a leap year if it is a multiple of 400. It is also a leap year if it is a multiple of 4 and not a multiple of 100.
 */

import java.util.Scanner;

public class DaysInMonth {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int month;
        int year;
        int days;
        String monthName;

        System.out.print("Enter a month number (1 = january): ");
        month = keyboard.nextInt();
        while (month < 1 || month > 12) {
            System.out.print("Invalid input. Please enter a number between 1 and 12. ");
            month = keyboard.nextInt();
        }
        System.out.print("Enter a year (4 digits): ");
        year = keyboard.nextInt();
        while ((int) (Math.log10(year) + 1) != 4) {
            System.out.print("Invalid input. Only years between 1000 and 9999 are supported. :( ");
            year = keyboard.nextInt();
        }

        monthName = switch(month) {
            case 1 -> "January";
            case 2 -> "February";
            case 3 -> "March";
            case 4 -> "April";
            case 5 -> "May";
            case 6 -> "June";
            case 7 -> "July";
            case 8 -> "August";
            case 9 -> "September";
            case 10 -> "October";
            case 11 -> "November";
            case 12 -> "December";
            default -> "Invalid month";
        };

        days = switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> 31;
            case 4, 6, 9, 11 -> 30;
            case 2 -> year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) ? 29 : 28;
            default -> 0;
        };
        System.out.printf("In %d, %s has %d days", year, monthName, days);

        keyboard.close();
    }
}
