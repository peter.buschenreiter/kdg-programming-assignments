package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * Ask the user for two numbers.
 * <p>
 * Print all numbers between 1 and 1000 that are divisible by both numbers (the remainder of dividing is 0)
 * <p>
 * Modify the program to print maximum 10 numbers on a line
 * <p>
 * Ask the user for a new set of dividers until the user enters a zero if the user enters a negative number for one of
 * the dividers, print an error message and restart the loop
 */

public class Divisible {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int num1;
        int num2;
        final int LOWERBOUND = 1;
        final int UPPERBOUND = 1_000;
        int printCounter;

        System.out.printf("We'll print all numbers from %d to %d that are divisible between 2 numbers of your choosing.\n", LOWERBOUND, UPPERBOUND);
        while (true) {
            System.out.println();
            System.out.print("Enter the first divider (end the program with 0): ");
            num1 = keyboard.nextInt();
            keyboard.close();
            if (num1 == 0) return;
            System.out.print("Enter the second divider: ");
            num2 = keyboard.nextInt();
            keyboard.close();
            if (num2 == 0) return;

            if (num1 <= 0 || num2 <= 0) {
                System.out.println("Please enter positive numbers");
                continue;
            }
            printCounter = 0;
            for (int i = Math.max(Math.max(num1, num2) + 1, LOWERBOUND); i < UPPERBOUND; i++) {
                if (i % num1 == 0 && i % num2 == 0) {
                    if (printCounter > 0 && printCounter % 10 == 0) System.out.println();
                    System.out.printf("%3d ", i);
                    printCounter++;
                }
            }
            System.out.println();
        }
    }
}
