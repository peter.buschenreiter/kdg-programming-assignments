package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Task 2: do-while
 * Make a copy of the program you wrote in task 1 and replace all while loops with do-while loops.
 */

public class DoWhileLoop {
    public static void main(String[] args) {
        int descending = 120;
        int multiples = 3;
        int exponent = 0;
        final int BASE = 5;
        char letter = 'A';

        do {
            System.out.printf("%d ", descending--);
        } while (descending >= 100);

        System.out.println();

        do {
            System.out.printf("%d ", multiples);
            multiples += 3;
        } while (multiples < 50);

        System.out.println();

        do {
            System.out.printf("%.0f ", Math.pow(BASE, exponent++));
        } while (Math.pow(BASE, exponent) < 10_000);

        System.out.println();

        do {
            System.out.printf("%c ", letter++);
        } while (letter <= 'Z');
    }
}
