package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Write a program that
 *
 * Displays a line with all numbers from 120 to 100 (descending)
 * Displays a line with all multiples of 3 below 50
 * Displays a line with the powers of 5 below 10,000
 * Displays a line with the alphabet
 * Hint: you can increment a char
 *
 */

public class WhileLoop {
    public static void main(String[] args) {
        int descending = 120;
        int multiples = 0;
        int exponent = 0;
        final int BASE = 5;
        char letter = 'A';

        while (descending >= 100) {
            System.out.printf("%d ", descending--);
        }
        System.out.println();
        while ((multiples += 3) < 50) {
            System.out.printf("%d ", multiples);
        }
        System.out.println();
        while (Math.pow(BASE, exponent) < 10_000) {
            System.out.printf("%.0f ", Math.pow(BASE, exponent++));
        }
        System.out.println();
        while (letter <= 'Z') {
            System.out.printf("%c ", letter++);
        }
    }
}
