package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * Draw a filled box using an ascii character.
 *<p>
 * Ask the user for the character to use, the height and the width. <br>
 * width is between 2 and 60, height is between 2 and 20. Check and ask again if if needed.<br>
 * Draw as many lines as the height. Use a loop.<br>
 * For each line, draw as many characters on a line as the width. You'll need another loop within he first one.
 * </p>
 * Modify the previous program to only draw the box borders.
 *<p>
 * The contents of the box should be blank.
 */

public class ASCII_Box {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        char symbol;
        int width;
        int height;
        final int MIN = 2;
        final int MAX_HEIGHT = 20;
        final int MAX_WIDTH = 60;

        System.out.println("We'll draw an ASCII box using a character and dimensions of your choice.");
        System.out.println();
        System.out.print("Enter a character: ");
        symbol = keyboard.next().charAt(0);
        System.out.printf("Enter the width (%d..%d): ", MIN, MAX_WIDTH);
        width = HelpMe.readNumberBetweenBounds(MIN, MAX_WIDTH, "width");
        System.out.printf("Enter the height (%d..%d): ", MIN, MAX_HEIGHT);
        height = HelpMe.readNumberBetweenBounds(MIN, MAX_HEIGHT, "height");

        for (int rows = 0; rows < height; rows++) {
            for (int cols = 0; cols < width; cols++) {
                if (rows == 0 || rows == height - 1 || cols == 0 || cols == width - 1) System.out.printf("%c", symbol);
                else System.out.print(" ");
            }
            System.out.println();
        }
        keyboard.close();
    }
}
