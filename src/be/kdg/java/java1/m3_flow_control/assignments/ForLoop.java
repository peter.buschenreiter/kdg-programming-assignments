package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Task 3: for
 * Make a copy of the program you wrote in task 1 and replace all while loops with for loops.
 */

public class ForLoop {
    public static void main(String[] args) {
        final int BASE = 5;

        for (int descending = 120; descending >= 100; descending--) {
            System.out.printf("%d ", descending);
        }

        System.out.println();

        for (int multiples = 3; multiples < 50; multiples+=3) {
            System.out.printf("%d ", multiples);
        }

        System.out.println();

        for (int exponent = 0; Math.pow(BASE, exponent) < 10_000; exponent++) {
            System.out.printf("%.0f ", Math.pow(BASE, exponent));
        }

        System.out.println();

        for (char letter = 'A'; letter < 'Z'; letter++) {
            System.out.printf("%c ", letter);
        }
    }
}
