package be.kdg.java.java1.m2_variables_operators.assignments;

/**
  *Write a program in which you declare and initialise variables of types: boolean, char, byte, short, int , long, float and
  *double. You're free to choose any value when initialising these variables.
  *
  *Next, print the value of each variable. Compile your program, execute it and check the output.
  */

public class Variables {
    public static void main(String[] args) {
        boolean bool = true;
        char letter = 'A';
        byte byteNum = 12;
        short shortNum = 4_327;
        int intNum = 7_689_798;
        long longNum = 787_787_438_344L;
        float floatNum = 433.8f;
        double doubleNum = 37_873_434.422;

        final double PI = 3.14;
        // PI = 3.141 not possible because of keyword 'final'

        System.out.println(bool);
        System.out.println(letter);
        System.out.println(byteNum);
        System.out.println(shortNum);
        System.out.println(intNum);
        System.out.println(longNum);
        System.out.println(floatNum);
        System.out.println(doubleNum);
        System.out.println(PI);
    }
}
