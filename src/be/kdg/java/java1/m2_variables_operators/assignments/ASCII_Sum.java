package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program that calculates the sum of the decimal codes of each character in a given string of text (including spaces
 * and other "special" characters).
 */

import java.util.Scanner;

public class ASCII_Sum {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String sentence;
        int sum = 0;

        System.out.print("Enter a string of text: ");
        sentence = keyboard.nextLine();

        for (int c = 0; c < sentence.length(); c++) {
            sum += (int) sentence.charAt(c);
        }
        System.out.println("Sum = " + sum);
        keyboard.close();
    }
}
