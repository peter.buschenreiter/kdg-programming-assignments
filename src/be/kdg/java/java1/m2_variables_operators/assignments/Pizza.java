package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program to calculate the total of a pizza order.
 *
 * The price of a single pizza is €8. Declare a constant of type int with a value of 800.
 * The price of an extra topping is €0.50. Declare a constant of type int with a value of 50.
 * Ask the user how many pizzas (s)he'd like.
 * Create a loop in which you ask the user how many toppings each pizza should have.
 * Print the total in euros.
 */

import java.util.Scanner;

public class Pizza {
    public static void main(String[] args) {
        final int PRICE_PIZZA = 800;
        final int PRICE_TOPPING = 50;
        double totalPrice;
        int pizzaAmount;
        int toppingsAmount;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("How many pizzas would you like to have? ");
        pizzaAmount = keyboard.nextInt();
        totalPrice = pizzaAmount * PRICE_PIZZA;
        for (int pizza = 1; pizza <= pizzaAmount; pizza++) {
            System.out.printf("How many toppings would you like on your pizza #%d? ", pizza);
            toppingsAmount = keyboard.nextInt();
            totalPrice += toppingsAmount * PRICE_TOPPING;
        }
        System.out.printf("Your total price is %.2f€", totalPrice / 100);

        keyboard.close();

    }
}
