package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.Scanner;

/**
 * Write a program that can encode and decode messages just like Julius Cesar did.
 *
 * Read an input message and convert it to upper case.
 * Shift each letter a certain number of places within the alphabet.
 * For example, shifting an 'A' 3 places makes it a 'D'.
 * When you reach the end of the alphabet, go back to 'A'. So a 'Z' becomes a 'C' when shifted 3 places.
 * Ignore spaces, just print them as they appear in the input.
 * Finally also decode the message applying an inverse operation.
 */

public class Encoding {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        StringBuilder encoded = new StringBuilder();
        StringBuilder decoded = new StringBuilder();
        String message;
        byte displacement;
        char newASCII;

        System.out.print("Enter a message to be decoded: ");
        message = keyboard.nextLine();
        message = message.toUpperCase();
        System.out.print("Displacement to be used: ");
        displacement = keyboard.nextByte();

        // encoding
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) == ' ') {
                encoded.append(' ');
                continue;
            }
            newASCII = (char) ((message.charAt(i) - 65 + displacement) % 26 + 65);
            encoded.append(newASCII);
        }

        // decoding
        for (int i = 0; i < encoded.length(); i++) {
            if (encoded.charAt(i) == ' ') {
                decoded.append(' ');
                continue;
            }
            newASCII = (char) ((encoded.charAt(i) - 65 - displacement + 26) % 26 + 65); //add 26 to ensure positive num before % operation
            decoded.append(newASCII);
        }
        System.out.println(encoded);
        System.out.println(decoded);

        keyboard.close();
    }
}
