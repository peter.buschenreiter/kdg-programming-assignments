package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.Scanner;

/**
 * Write a program in which you ask the user for a number that is made up of exactly four digits. Calculate the sum of these
 * four digits.
 *
 * Make sure that the number entered by the user is made up of four digits, so it should be in the range [1000..9999]. Make
 * use of constants MINIMUM = 1000 and MAXIMUM = 9999.
 *
 * Possible extras:
 *
 * Make sure that the program repeats until the user enters -1 (you can use a boolean variable to help you)
 * Allow the user to enter the amount of digits so that it's not always supposed to be a 4-digit number.
 */

public class DigitsV2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int amountDigits;
        int num = 0;
        int sum = 0;

        System.out.print("Enter the amount of digits the numbers should have: ");
        amountDigits = keyboard.nextInt();
        System.out.printf("This program calculates the sum of %d-digit numbers the user inputs.\n", amountDigits);

        final int MINIMUM = (int) Math.pow(10, (amountDigits - 1)); // 2 --> 10 | 1 --> 1
        final int MAXIMUM = (int) Math.pow(10, amountDigits) - 1; // 2 --> 99

        // Reading in digits until input is -1
        while (num != -1) {
            sum += sumOfDigits(num);
            System.out.printf("Enter a %d-digit number (stop with -1): ", amountDigits);
            num = keyboard.nextInt();
            while (num < MINIMUM || num > MAXIMUM) { // checking if number is out of bounds
                if (num == -1) break;
                System.out.printf("Invalid input! Digit needs to be a %d-digit number.\n", amountDigits);
                System.out.print("Enter a valid input: ");
                num = keyboard.nextInt();
            }
        }
        System.out.println("The sum of the digits of this number is " + sum);

        keyboard.close();
    }
    public static int sumOfDigits(int num) {
        int sum = 0;
        while (num >= 1) { // while the number has digits left (runs the last time at the last digit, because 9/10=0)
            sum += num % 10; // add the rightmost digit to the sum
            num /= 10; // cut off the rightmost digit
        }
        return sum;
    }
}
