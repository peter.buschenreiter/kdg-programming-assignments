package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.Scanner;

/**
 * Write a program to simulate three switches. Ask for the state of each switch by reading a literal of type boolean.
 *
 * Next, perform the following checks in sequence:
 *
 * Check if at least two switches are turned on (on = true)
 * Check if exactly two switches are turned on
 * Check if at most one switch is turned on
 */

public class Switches {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        boolean switch1;
        boolean switch2;
        boolean switch3;

        System.out.print("Enter the state of switch 1 (true or false): ");
        switch1 = keyboard.nextBoolean();
        System.out.print("Enter the state of switch 2 (true or false): ");
        switch2 = keyboard.nextBoolean();
        System.out.print("Enter the state of switch 3 (true or false): ");
        switch3 = keyboard.nextBoolean();

        System.out.println("Are at least two switches on? " + areAtLeastTwoOn(switch1, switch2, switch3));
        System.out.println("Are exactly two switches on? " + areExactlyTwoOn(switch1, switch2, switch3));
        System.out.println("Is at most one switch on? " + isAtMostOneOn(switch1, switch2, switch3));

        keyboard.close();
    }
    public static boolean areAtLeastTwoOn(boolean switch1, boolean switch2, boolean switch3) {
        return (switch1 && switch2 || switch2 && switch3 || switch1 && switch3);
    }
    public static boolean areExactlyTwoOn(boolean switch1, boolean switch2, boolean switch3) {
        return (switch1 && switch2 && !switch3 || switch1 && !switch2 && switch3 || !switch1 && switch2 && switch3);
    }
    public static boolean isAtMostOneOn(boolean switch1, boolean switch2, boolean switch3) {
        return (switch1 && !switch2 && !switch3 || !switch1 && !switch2 && switch3 || !switch1 && switch2 && !switch3);
    }
}
