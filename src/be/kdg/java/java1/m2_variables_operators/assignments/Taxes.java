package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program that adds VAT (value-added tax) to a monetary value or that calculates the VAT-exclusive amount from a
 * monetary value that already includes VAT. See the examples below.
 *
 * Ask the user to enter the following data:
 *
 * - The VAT percentage
 * - The amount (may or may not include VAT)
 *
 * You should also give the user the option to indicate whether the amount entered already includes VAT. Display an
 * error message in case the user entered an invalid option.
 */

import java.util.Scanner;

public class Taxes {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int vatPercentage;
        double amount;
        double result = 0;
        int choice;

        System.out.print("Enter the VAT percentage: ");
        vatPercentage = keyboard.nextInt();
        System.out.print("Enter an amount in €: ");
        amount = keyboard.nextDouble();
        System.out.print("Make a choice (1 = inclusive, 2 = exclusive): ");
        choice = keyboard.nextInt();

        switch (choice) {
            case 1 -> {
                result = amount / (100 + (double) vatPercentage) * 100;
                result = Math.round(result * 100) / 100.0;
            }
            case 2 -> result = amount / 100 * vatPercentage;
            default -> System.out.println("Invalid choice!");
        }
        System.out.printf("%.2f€ + %d%% VAT = %.2f€", amount, vatPercentage, result);

        keyboard.close();
    }
}
