package m1;

import java.util.Scanner;

public class Summation {
    public static void main(String[] args) {
        int num = 10;
        int sum = 0;
        int counter = 0;
        Scanner keyboard = new Scanner(System.in);

        while (num != 0) {
            counter++;
            System.out.print("Enter a number (0 to terminate the programma): ");
            num = keyboard.nextInt();
            sum += num;
        }
        if (counter - 1 == 1) System.out.println("Only " + (counter - 1) + " number was input: " + sum);
        else System.out.println("The sum of these " + (counter - 1) + " numbers is: " + sum);
    }
}