package m1;

import java.util.Scanner;


public class Product {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int num1;
        int num2;
        int num3;
        int product;

        System.out.println("Enter a number: ");
        num1 = keyboard.nextInt();
        System.out.println("Enter another number: ");
        num2 = keyboard.nextInt();
        System.out.println("Enter a final number: ");
        num3 = keyboard.nextInt();
        product = num1 * num2 * num3;
        System.out.println("The product is: " + product);
    }
}
