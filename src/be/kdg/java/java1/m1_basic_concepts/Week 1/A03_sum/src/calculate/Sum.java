package calculate;

import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        int sum;
        int num1;
        int num2;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a number: ");
        num1 = keyboard.nextInt();
        System.out.println("Enter another number: ");
        num2 = keyboard.nextInt();
        sum = num1 + num2;
        System.out.println("The sum is: " + sum);
    }
}
