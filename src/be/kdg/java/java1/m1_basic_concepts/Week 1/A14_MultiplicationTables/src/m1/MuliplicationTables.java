package m1;

import java.util.Scanner;

class MultiplicationTables {
    public static void main(String[] args) {
        int base;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Which multiplication table would you like to see? ");
        base = keyboard.nextInt();

        //print table
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + " x " + base + " = " + (base * i));
        }
    }
}
