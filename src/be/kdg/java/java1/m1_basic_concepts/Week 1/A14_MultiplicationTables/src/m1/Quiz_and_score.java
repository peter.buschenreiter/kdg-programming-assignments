package m1;

import java.util.Scanner;

public class Quiz_and_score {
    public static void main(String[] args) {
        int num;
        int result;
        int guess;
        int counter = 0;
        String correctOrWrong;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Which multiplication table would you like to practice? ");
        num = keyboard.nextInt();
        for (int i = 1; i <= 10; i++) {
            result = i * num;
            System.out.print(i + " x " + num + " = ");
            guess = keyboard.nextInt();
            correctOrWrong = (result == guess) ? "Correct!" : "Wrong!";
            counter += (result != guess) ? 1 : 0;
            System.out.println(correctOrWrong);
        }
        if (counter == 0) {
            System.out.println("Perfect score!");
        } else {
            System.out.println("Oops. You made " + counter + " errors. Try again!");
        }
    }
}