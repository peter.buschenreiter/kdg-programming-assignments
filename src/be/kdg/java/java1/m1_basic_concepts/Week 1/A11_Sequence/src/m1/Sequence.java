package m1;

import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {
        int amount = 0;
        int startVal;
        int inc;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("How many numbers do you want to print? ");
        amount = keyboard.nextInt();
        System.out.print("What is the starting value? ");
        startVal = keyboard.nextInt();
        System.out.print("What is the increment? ");
        inc = keyboard.nextInt();
        for (int i = 0; i < amount; i++) {
            System.out.print(startVal + " ");
            startVal += inc;
        }
    }
}
