package m1;

import java.time.LocalDate;
import java.util.Scanner;

public class Age {
    public static void main(String[] args) {
        String name;
        int year;
        int age;

        int currentYear = LocalDate.now().getYear();
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your name: ");
        name = keyboard.nextLine();
        System.out.println("Dear " + name + ", please enter the year you were born: ");
        year = keyboard.nextInt();
        age = currentYear - year;
        System.out.println("If all goes well you`ll be " + age + " by the end of the year.");
    }
}
