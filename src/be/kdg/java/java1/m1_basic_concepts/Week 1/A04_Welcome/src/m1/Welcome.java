package m1;

import java.util.Scanner;

public class Welcome {
    public static void main(String[] args) {
        String name;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What is your name? ");
        name = keyboard.nextLine();
        System.out.println("Welcome " + name + "!");
    }
}
