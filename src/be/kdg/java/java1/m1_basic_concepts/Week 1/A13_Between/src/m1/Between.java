package m1;

import java.util.Scanner;

public class Between {
    public static void main(String[] args) {
        int num1;
        int num2;
        int num3;
        int LOWER_BOUND = 1;
        int UPPER_BOUND = 100;
        int result;

        System.out.print("Enter the first number between " + LOWER_BOUND + " and " + UPPER_BOUND + ": ");
        num1 = getNumberInBounds();
        System.out.print("Enter the second number between " + LOWER_BOUND + " and " + UPPER_BOUND + ": ");
        num2 = getNumberInBounds();
        System.out.print("Enter the third number between " + LOWER_BOUND + " and " + UPPER_BOUND + ": ");
        num3 = getNumberInBounds();
        result = Math.min(Math.max(num1,num2),num3);
        System.out.println("The middle number is: " + result);
    }
    public static int getNumberInBounds() {
        int LOWER_BOUND = 1;
        int UPPER_BOUND = 100;
        Scanner keyboard = new Scanner(System.in);
        int result = keyboard.nextInt();

        while (result < LOWER_BOUND || result > UPPER_BOUND) {
            System.out.println("Out of bounds. Try again!");
            result = keyboard.nextInt();
        }
        return result;
    }
}
