package be.kdg.java.java2.m7.assignments.a03product;

public class Book extends Product{
    private String title;
    private String author;

    public Book(String code, String description, double price, String title, String author) {
        super(code, description, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public double getVat() {
        return 0.06;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Book: %s by %s \n", getTitle(), getAuthor());
    }
}
