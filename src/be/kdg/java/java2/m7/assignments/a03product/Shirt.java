package be.kdg.java.java2.m7.assignments.a03product;

public class Shirt extends Product {
    private String size;
    private String gender;

    public Shirt(String code, String description, double price, String size, String gender) {
        super(code, description, price);
        this.size = size;
        this.gender = gender;
    }

    public String getSize() {
        return size;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Shirt: %s for %s \n", getSize(), getGender());
    }
}
