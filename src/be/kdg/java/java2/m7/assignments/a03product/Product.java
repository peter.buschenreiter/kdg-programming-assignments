package be.kdg.java.java2.m7.assignments.a03product;

public class Product {
    protected String code;
    protected String description;
    protected double price;

    public Product(String code, String description, double price) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public double getPrice() {
        return price * (1 + getVat());
    }

    public double getVat() {
        return 0.21;
    }

    @Override
    public String toString() {
        return String.format("Code: %-10s Description: %-25s Price: %.2f€ \n", getCode(), description, getPrice());
    }
}
