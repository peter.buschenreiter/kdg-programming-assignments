package be.kdg.java.java2.m7.assignments.a03product;

public class TestProduct {
    public static void main(String[] args) {
        Product product = new Product("pp", "LoNg Pp", 31.5);
        Book book = new Book("harry potter 1", "harry potter book", 19.99, "Harry Potter and the philosopher's stone", "J. K. Rowling");
        Shirt shirt = new Shirt("shirt", "hawaiian shirt", 50.49, "XXXL", "F");
        Camera camera = new Camera("Canon camera", "Canon EOS 4000D", 399.99, 18_000_000);

        System.out.println(product);
        System.out.println(book);
        System.out.println(shirt);
        System.out.println(camera);
    }
}
