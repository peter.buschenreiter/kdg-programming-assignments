package be.kdg.java.java2.m7.assignments.a03product;

public class Camera extends Product {
    private int pixels;

    public Camera(String code, String description, double price, int pixels) {
        super(code, description, price);
        this.pixels = pixels;
    }

    public int getPixels() {
        return pixels;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Camera: %d pixels \n", getPixels());
    }
}
