package be.kdg.java.java2.m7.assignments.a11animals_polimorphism;

public class Rabbit extends Animal {
    private boolean digs;

    public Rabbit (String name, String breed, String color, String CHIP_NUMBER, boolean digs) {
        super(name, breed, color, CHIP_NUMBER);
        super.setTagLine("I'm an ice rabbit");
        this.digs = digs;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("%s", digs ? "that digs." : ".");
    }
}
