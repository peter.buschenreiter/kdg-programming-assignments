package be.kdg.java.java2.m7.assignments.a07animals;

public class TestAnimal {
    public static void main(String[] args) {
        Dog scoobyDoo = new Dog("Scooby-Doo", "Great Dane", "brown", "19690913");
        Rabbit chungus = new Rabbit("Big Chungus", "Flemish Giant", "grey/white", "19400727", true);

        System.out.println(scoobyDoo);
        System.out.println(scoobyDoo.getTagLine());

        System.out.println(chungus);
        System.out.println(chungus.getTagLine());

    }
}
