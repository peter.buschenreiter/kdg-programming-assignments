package be.kdg.java.java2.m7.assignments.a14student_contact;

public class Student extends Person {
	private int number;

	public Student(int number, String name, String email, String fixed, String mobile) {
		super(name, email, fixed, mobile);
		this.number = number;
	}
}
