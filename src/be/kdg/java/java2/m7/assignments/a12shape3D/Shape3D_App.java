package be.kdg.java.java2.m7.assignments.a12shape3D;

public class Shape3D_App {
    public static void main(String[] args) {
        ShapeCatalog catalog = new ShapeCatalog();
        catalog.fillCatalog();
        catalog.showShapes();
    }
}
