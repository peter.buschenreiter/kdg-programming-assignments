package be.kdg.java.java2.m7.assignments.a12shape3D;

public class Cylinder extends Shape3D {
    private double radius = 1.0;
    private double length = 1.0;

    public Cylinder() {}

    public Cylinder(String colour, double radius, double length) {
        super(colour);
        this.radius = radius;
        this.length = length;
    }

    public Cylinder(double radius, double length) {
        this.radius = radius;
        this.length = length;
    }

    @Override
    public double volume() {
        return Math.PI * radius * radius * length;
    }

    @Override
    public double surface() {
        final double PI = Math.PI;
        return 2 * PI * radius * length + 2 * PI * radius * radius;
    }
}
