package be.kdg.java.java2.m7.assignments.a15garage;

public class Garage {
private String name;

public Garage(String name) {
	this.name = name;
}

public String getName() {
	return name;
}

@Override
public String toString() {
	return String.format("Garage: %s", getName());
}
}
