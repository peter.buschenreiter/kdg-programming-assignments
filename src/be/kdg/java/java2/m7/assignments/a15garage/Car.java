package be.kdg.java.java2.m7.assignments.a15garage;

public class Car {
private Garage garage;
private String brand;

public Car(String brand, Garage garage) {
	this.garage = garage;
	this.brand = brand;
}

public Car(Car car) {
	this(car.brand, car.garage);
}

public Car(String brand) {
	this(brand, null);
}

public void setGarage(Garage garage) {
	this.garage = garage;
}

@Override
public String toString() {
	return String.format("Car: %s Garage: %s", brand, garage);
}
}
