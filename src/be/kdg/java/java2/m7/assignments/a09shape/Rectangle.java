package be.kdg.java.java2.m7.assignments.a09shape;

public class Rectangle extends Shape {
    protected int height;
    protected int width;

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public Rectangle() {
    }

    public Rectangle(int x, int y) {
        super(x, y);
    }

    public Rectangle(Rectangle rectangle) {
        this(rectangle.x, rectangle.y, rectangle.height, rectangle.width);
    }

    public double getArea() {
        return width * height;
    }

    @Override
    public double getPerimeter() {
        return 2 * width + 2 * height;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Perimeter: %.2f \t Area: %.2f", getPerimeter(), getArea());
    }
}
