package be.kdg.java.java2.m7.assignments.a13student;

public class Person {
    private String name;
    protected Phone phone;

    public Person(String phone, String name) {
        this.phone = new Phone(phone);
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Name: %s Phone: %s", name, phone.number);
    }
}
