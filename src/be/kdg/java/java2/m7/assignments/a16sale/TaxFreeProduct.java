package be.kdg.java.java2.m7.assignments.a16sale;

public class TaxFreeProduct extends Product {
	public TaxFreeProduct(int number, String description, double price) {
		super( number, description, price, 0);
	}
}
