package be.kdg.java.java2.m7.assignments.a16sale;

import java.util.Arrays;

public class Sale {
	private final static int MAX_LINES = 100;
	SalesLine[] lines = new SalesLine[MAX_LINES];
	private int nrOfSalesLines = 0;

	public int getNrOfSalesLines() {
		return nrOfSalesLines;
	}

	public void add(SalesLine salesLine) {
		if (nrOfSalesLines >= MAX_LINES) {
			System.out.println("Maximum amount of lines reached!");
			return;
		}
		for (int i = 0; i < nrOfSalesLines; i++) {
			if (salesLine.equals(lines[i])) {
				lines[i].addQuantity(salesLine.getQuantity());
				return;
			}
		}
		lines[nrOfSalesLines] = salesLine;
		nrOfSalesLines++;
	}

	public void printTotal() {
		double total = 0;
		for (int i = 0; i < nrOfSalesLines; i++) {
			total += lines[i].getPrice();
		}
		System.out.printf("Total VAT exclusive: €%.2f", total);
	}

	public void printVatTotal() {
		double total = 0;
		for (int i = 0; i < nrOfSalesLines; i++) {
			total += lines[i].getVat();
		}
		System.out.printf("VAT                : €%6.2f", total);
	}

	public void printTotalVatInclusive() {
		double total = 0;
		for (int i = 0; i < nrOfSalesLines; i++) {
			total += lines[i].getPriceVatInclusive();
		}

		System.out.printf("Total VAT inclusive: €%.2f", total);
	}

	public void printTotalImportTaxes() {
		double total = 0;
		for (int i = 0; i < nrOfSalesLines; i++) {
			total += lines[i].getProduct().getImportTax();
		}
		System.out.printf("Total import taxes : € %.2f", total);
	}

	public void printInvoice() {
		System.out.println("\n---IMPORT TAXES---\n");
		for (int i = 0; i < nrOfSalesLines; i++) {
			lines[i].show();
		}
		System.out.println();
		printTotal();
		System.out.println();
		printVatTotal();
		System.out.println();
		printTotalVatInclusive();
		System.out.println();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < nrOfSalesLines; i++) {
			sb.append(lines[i].toString()).append("\n");
		}
		return sb.toString();
	}
}
