package be.kdg.java.java2.m8.assignments.a03animals;

public class Eagle implements Named, EggLaying, Flying {
	private String name;
	private int numberOfEggsPerYear;
	private int maxSpeed;
	private int divingSpeed;

	public Eagle(String name, int numberOfEggsPerYear, int maxSpeed, int divingSpeed) {
		this.name = name;
		this.numberOfEggsPerYear = numberOfEggsPerYear;
		this.maxSpeed = maxSpeed;
		this.divingSpeed = divingSpeed;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getNumberOfEggsPerYear() {
		return numberOfEggsPerYear;
	}

	@Override
	public int getMaxFlyingSpeed() {
		return maxSpeed;
	}

	public int getDivingSpeed() {
		return divingSpeed;
	}

	@Override
	public String toString() {
		return String.format("Name: %s %nEggs: %d %nSpeed: %d %nDiving speed: %d %n", getName(), getNumberOfEggsPerYear(), getMaxFlyingSpeed(), getDivingSpeed());
	}
}
