package be.kdg.java.java2.m8.assignments.a03animals;

public class Swallow implements Named, EggLaying, Flying {
	private String name;
	private int numberOfEggsPerYear;
	private int maxSpeed;

	public Swallow(String name, int numberOfEggsPerYear, int maxSpeed) {
		this.name = name;
		this.numberOfEggsPerYear = numberOfEggsPerYear;
		this.maxSpeed = maxSpeed;
	}

	@Override
	public int getNumberOfEggsPerYear() {
		return numberOfEggsPerYear;
	}

	@Override
	public int getMaxFlyingSpeed() {
		return maxSpeed;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("Name: %s %nEggs: %d %nSpeed: %d %n", getName(), getNumberOfEggsPerYear(), getMaxFlyingSpeed());
	}
}
