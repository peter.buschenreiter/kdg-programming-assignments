package be.kdg.java.java2.m8.assignments.a09books;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Connection connection = initialiseDatabase();
		if (connection == null) {
			System.exit(1);
		}

		Scanner scanner = new Scanner(System.in);
		Integer choice = null;
		do {
			System.out.println("What would you like to do?");
			System.out.println("1) Display all books");
			System.out.println("2) Add a book");
			System.out.println("3) Search books by title");
			System.out.println("4) Quit");
			System.out.print(">> ");
			do {
				try {
					try {
						if (choice > 4 || choice < 1) {
							System.out.print("Input needs to between 1 and 4: ");
						}
					} catch (NullPointerException e) {
						// yeet the nullpointer outta here
					}
					choice = scanner.nextInt();
				} catch (InputMismatchException e) {
					System.out.println("Input needs to be a number");
				}
				scanner.nextLine(); // clear the input buffer
			} while (choice == null || choice > 4 || choice < 1);

			switch (choice) {
				case 1 -> displayAllBooks(connection);
				case 2 -> {
					System.out.print("Title? \t\t");
					String title = scanner.nextLine();
					System.out.print("Author? \t");
					String author = scanner.nextLine();
					System.out.print("Published?\t");
					int yearPublished;
					try {
						yearPublished = scanner.nextInt();
						addBook(connection, new Book(title, author, yearPublished));
					} catch (InputMismatchException e) {
						System.out.println("Publishing year needs to be a number.");
					}
				}
				case 3 -> {
					System.out.print("Search term? ");
					String searchTerm = scanner.nextLine();
					searchBooksByTitle(connection, searchTerm);
				}
			}
		} while (choice != 4);

		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Closing connection failed!");
		}
	}

	private static Connection initialiseDatabase() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/booksDB", "postgres", "anubis512");
			Statement statement = connection.createStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS books (" +
					"title VARCHAR(10), " +
					"author VARCHAR(10), " +
					"year_first_published INTEGER," +
					"CONSTRAINT pk_books PRIMARY KEY (title, author))"
			);
			//			statement.executeUpdate("DELETE FROM books");
			statement.close();
		} catch (SQLException e) {
			System.out.println("DB initialization failed!");
			return null;
		}
		return connection;
	}

	private static void displayAllBooks(Connection connection) {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM books");
			if (resultSet.next()) {
				do {
					String title = resultSet.getString(1);
					String author = resultSet.getString(2);
					int yearFirstPublished = resultSet.getInt(3);
					Book book = new Book(title, author, yearFirstPublished);
					System.out.println(book);
				} while (resultSet.next());
			} else {
				System.out.println("<no books found>\n");
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			System.out.println("Displaying all books failed!\n");
		}
	}

	private static void addBook(Connection connection, Book book) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("INSERT INTO books (title, author, year_first_published) VALUES ('" + book.getTitle() + "','" + book.getAuthor() + "', '" + book.getYearFirstPublished() + "')");
			System.out.println("\nAdded the following book:");
			System.out.println(book);
			statement.close();
		} catch (SQLException e) {
			System.out.println("Adding book failed!");
			System.out.println(e.getMessage());
			System.out.println();
		}

	}

	private static void searchBooksByTitle(Connection connection, String searchTerm) {
		searchTerm = searchTerm.toLowerCase();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM books WHERE lower(title) LIKE '%" + searchTerm + "%'");
			if (resultSet.next()) {
				do {
					String title = resultSet.getString(1);
					String author = resultSet.getString(2);
					int yearFirstPublished = resultSet.getInt(3);
					Book book = new Book(title, author, yearFirstPublished);
					System.out.println(book);
				} while (resultSet.next());
			} else {
				System.out.printf("<no books containing \"%s\" found>\n\n", searchTerm);
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			System.out.println("Searching for books failed!");
			e.getMessage();
			System.out.println();
		}
	}

}
