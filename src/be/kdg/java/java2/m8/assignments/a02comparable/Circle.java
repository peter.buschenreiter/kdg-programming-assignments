package be.kdg.java.java2.m8.assignments.a02comparable;

public class Circle extends Shape {
	private int radius;

	public Circle(int x, int y, int radius) {
		super(x, y);
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}

	@Override
	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}

	@Override
	public boolean isEqualTo(Object o) {
		if (!(o instanceof Circle circle)) return false;
		if (this == o) return true;
		return radius == circle.radius;
	}

	@Override
	public boolean isGreaterThan(Object o) {
		if (!(o instanceof Circle circle)) return false;
		return getArea() > circle.getArea();
	}

	@Override
	public boolean isLessThan(Object o) {
		if (!(o instanceof Circle circle)) return false;
		return getArea() < circle.getArea();
	}

	@Override
	public void print() {
		System.out.println("Circle");
		System.out.println("======");
		System.out.printf("Position: \t(%d, %d) %n", getX(), getY());
		System.out.printf("Radius: \t%d %n", radius);
	}
}
