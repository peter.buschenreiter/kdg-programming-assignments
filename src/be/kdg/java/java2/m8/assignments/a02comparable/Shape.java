package be.kdg.java.java2.m8.assignments.a02comparable;

public abstract class Shape implements Printable, Comparable {
	private int x;
	private int y;

	public Shape(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setPosition(int x, int y) {
		setX(x);
		setY(y);
	}

	public abstract double getArea();

	public abstract double getPerimeter();
}
