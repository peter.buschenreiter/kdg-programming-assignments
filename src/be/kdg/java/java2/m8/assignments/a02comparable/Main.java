package be.kdg.java.java2.m8.assignments.a02comparable;

public class Main {
	public static void main(String[] args) {
		Car car1 = new Car("Peugeot", "208", "1-ABC-123");
		Car car2 = new Car("Peugeot", "208", "2-XYZ-123");
		Car car3 = new Car("Opel", "Corsa", "1-DEF-456");
		Rectangle rectangle1 = new Rectangle(10, 10, 100, 100);
		Rectangle rectangle2 = new Rectangle(0, 0, 100, 100);
		Rectangle rectangle3 = new Rectangle(-50, -50, 50, 200);
		Circle circle1 = new Circle(0, 0, 50);
		Circle circle2 = new Circle(100, 100, 50);
		Circle circle3 = new Circle(0, 0, 10);


		System.out.println("Testing the Car class");
		System.out.println("=====================");
		if (!car1.isEqualTo(car2)) System.out.println("1 This should print 'true': " + car1.isEqualTo(car2));
		if (car1.isEqualTo(car3)) System.out.println("2 This should print 'false': " + car1.isEqualTo(car3));
		if (car1.isGreaterThan(car2)) System.out.println("3 This should print 'false': " + car1.isGreaterThan(car2));
		if (!car1.isGreaterThan(car3)) System.out.println("4 This should print 'true': " + car1.isGreaterThan(car3));
		if (car1.isLessThan(car3)) System.out.println("5 This should print 'false': " + car1.isLessThan(car3));
		System.out.println();
		System.out.println("Testing the Rectangle class");
		System.out.println("===========================");
		if (!rectangle1.isEqualTo(rectangle2)) System.out.println("6 This should print 'true': " + rectangle1.isEqualTo(rectangle2));
		if (rectangle1.isEqualTo(rectangle3)) System.out.println("7 This should print 'false': " + rectangle1.isEqualTo(rectangle3));
		if (rectangle1.isGreaterThan(rectangle2)) System.out.println("8 This should print 'false': " + rectangle1.isGreaterThan(rectangle2));
		if (!rectangle1.isGreaterThan(rectangle3)) System.out.println("9 This should print 'true': " + rectangle1.isGreaterThan(rectangle3));
		if (rectangle1.isLessThan(rectangle3)) System.out.println("10 This should print 'false': " + rectangle1.isLessThan(rectangle3));
		System.out.println();
		System.out.println("Testing the Circle class");
		System.out.println("========================");
		if (!circle1.isEqualTo(circle2)) System.out.println("11 This should print 'true': " + circle1.isEqualTo(circle2));
		if (circle1.isEqualTo(circle3)) System.out.println("12 This should print 'false': " + circle1.isEqualTo(circle3));
		if (circle1.isGreaterThan(circle2)) System.out.println("13 This should print 'false': " + circle1.isGreaterThan(circle2));
		if (!circle1.isGreaterThan(circle3)) System.out.println("14 This should print 'true': " + circle1.isGreaterThan(circle3));
		if (circle1.isLessThan(circle3)) System.out.println("15 This should print 'false': " + circle1.isLessThan(circle3));
		System.out.println();
		System.out.println("Comparing different types");
		System.out.println("=========================");
		if (circle1.isEqualTo(rectangle1)) System.out.println("16 This should print 'false': " + circle1.isEqualTo(rectangle1));
		if (rectangle1.isEqualTo(car1)) System.out.println("17 This should print 'false': " + rectangle1.isEqualTo(car1));
		if (car1.isEqualTo(circle1)) System.out.println("18 This should print 'false': " + car1.isEqualTo(circle1));
	}

}
