package be.kdg.java.java2.m8.assignments.a10miniproject_sale;

public interface Vat_Transaction {
	double getImportTax();
	double getPrice();
	double getPriceVatInclusive();
}
