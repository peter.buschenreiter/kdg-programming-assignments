package be.kdg.java.java2.m8.assignments.a10miniproject_sale;

public class TaxFreeProduct extends Product {
	public TaxFreeProduct(int number, String description, double price) {
		super( number, description, price, 0);
	}
}
