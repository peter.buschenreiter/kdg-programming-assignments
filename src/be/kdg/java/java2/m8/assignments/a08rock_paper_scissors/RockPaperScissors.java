package be.kdg.java.java2.m8.assignments.a08rock_paper_scissors;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		Random rand = new Random();
		final String[] options = { "rock", "paper", "scissors" };
		int choice = 0;
		boolean isInputOK = false;
		int comp;

		while (true) {
			System.out.println("Rock(1), paper(2) or scissors(3)? (enter 0 to stop) ");
			do {
				System.out.print("Enter 0, 1, 2 or 3: ");
				try {
					choice = keyboard.nextInt();
					if (choice > 0 && choice <= 3) {
						isInputOK = true;
					} else if (choice == 0) return;
				} catch (InputMismatchException e) {
					keyboard.next();
					isInputOK = false;
				}
			} while (!isInputOK);
			comp = rand.nextInt(1, 4);
			System.out.printf("Your choice: \t%s %n", options[choice - 1]);
			System.out.printf("My choice: \t\t%s %n", options[comp - 1]);

			if (choice == comp) System.out.println("It's a tie!");
			else if ((choice + 3 - 2) % 3 == comp - 1) System.out.println("You won, congratulations!");
			else System.out.println("I won!");
			System.out.println();
		}
	}
}
