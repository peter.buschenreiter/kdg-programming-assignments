package be.kdg.java.java2.m8.assignments.a05pointsV2;

public class Point {
	private int x;
	private int y;
	private static final String COLOR = "red";
	private static int COUNT = 0;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		COUNT++;
	}

	public static int getCOUNT() {
		return COUNT;
	}

	public static String getCOLOR() {
		return COLOR;
	}

}
