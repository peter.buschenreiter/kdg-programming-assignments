package be.kdg.java.java2.m8.assignments.a01interfaces;

public class Rectangle extends Shape{
	private int width;
	private int height;

	public Rectangle(int x, int y, int width, int height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}

	@Override
	public double getArea() {
		return width * height;
	}

	@Override
	public double getPerimeter() {
		return 2 * width + 2 * height;
	}

	@Override
	public void print() {
		System.out.println("Rectangle");
		System.out.println("======");
		System.out.printf("Position: \t(%d, %d) %n", getX(), getY());
		System.out.printf("Width: \t\t%d %n", width);
		System.out.printf("Height: \t%d %n", height);
	}
}
