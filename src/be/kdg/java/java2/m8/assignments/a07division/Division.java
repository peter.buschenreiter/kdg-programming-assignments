package be.kdg.java.java2.m8.assignments.a07division;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Division {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int a = 0;
		int b = 0;
		int div = 0;
		boolean isInputOK = false;

		do {
			System.out.print("Please enter a value for 'a': ");
			try {
				a = keyboard.nextInt();
				isInputOK = true;
			} catch (InputMismatchException e) {
				keyboard.next();
				System.out.println("That's not a number!");
			}
		} while (!isInputOK);

		do {
			System.out.print("Please enter a value for 'b': ");
			try {
				b = keyboard.nextInt();
				isInputOK = true;
			} catch (InputMismatchException e) {
				keyboard.next();
				System.out.println("That's not a number!");
				isInputOK = false;
			}
			if (isInputOK) {
				try {
					div = a / b;
					isInputOK = true;
				} catch (ArithmeticException e) {
					System.out.println("Can't divide by zero!");
					isInputOK = false;
				}
			}
		} while (!isInputOK);
		System.out.printf("%d / %d = %d", a, b, div);
	}
}
