package be.kdg.java.java3.m1.assignments.a02listevennumbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**

 - Make a List numberList with 20 random numbers between 1 and 50 (both inclusive)<br>
 - Print the list without using a loop<br>
 - Copy the list to an array numberArray<br>
 - Print the array using a for-each loop<br>
 - Copy the array to a new List evenNumbers<br>
 - Remove all odd numbers from evenNumbers.<br>
 - The previous step might give problems. Try to solve them!<br>
 - Print evenNumbers and verify that your program works correctly.<br>
 */

public class ListEvenNumbers {
	public static void main(String[] args) {
		Random random = new Random();
		List<Integer> numberList = new ArrayList<>();

		for (int i = 0; i < 20; i++) {
			numberList.add(random.nextInt(1, 51));
		}

		System.out.println(numberList);

		Integer[] numberArray = numberList.toArray(Integer[]::new);

		for (Integer number : numberArray) {
			System.out.println(number);
		}

		List<Integer> evenNumbers = new ArrayList<>(Arrays.stream(numberArray).toList());

		evenNumbers.removeIf(num -> num % 2 == 1);

		System.out.println(evenNumbers);
	}
}
