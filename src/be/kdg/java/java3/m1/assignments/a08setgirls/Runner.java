package be.kdg.java.java3.m1.assignments.a08setgirls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Runner {
	public static void main(String[] args) {
		List<Girl> girls = new ArrayList<>(List.of(new Girl("An", 20),new Girl("Bea", 20),
				new Girl("Bea", 25), new Girl("Diana", 25),
				new Girl("Zoë", 18), new Girl("Ekaterina", 18),
				new Girl("Bea", 20)));

		System.out.println(girls);

		HashSet<Girl> hashedGirls = new HashSet<>(girls);

		System.out.printf("Set of %d girls: %s", hashedGirls.size(), hashedGirls);
	}
}
