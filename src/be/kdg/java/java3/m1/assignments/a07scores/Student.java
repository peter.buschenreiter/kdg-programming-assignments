package be.kdg.java.java3.m1.assignments.a07scores;

public class Student implements Comparable<Student> {
	private String name;
	private double score;

	public Student(String name, double score) {
		this.name = name;
		this.score = score;
	}

	@Override
	public int compareTo(Student o) {
		return Double.compare(this.score, o.score);
	}

	@Override
	public String toString() {
		return String.format("%-20s -> %5.02f", name, score);
	}
}
