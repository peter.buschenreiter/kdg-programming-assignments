package be.kdg.java.java3.m1.assignments.a07scores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Students {
	private List<Student> students;

	public Students(List<Student> students) {
		this.students = new ArrayList<>(students);
	}

	public void sortByScore(boolean asc) {
		Collections.sort(students);
		if (!asc) {
			Collections.reverse(students);
		}
	}

	public Student getHighScore() {
		return Collections.max(students);
	}

	public Student getLowScore() {
		return Collections.min(students);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Student student : students) {
			sb.append(student).append("\n");
		}
		return sb.toString();
	}
}
