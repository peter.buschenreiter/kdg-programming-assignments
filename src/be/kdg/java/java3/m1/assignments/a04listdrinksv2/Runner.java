package be.kdg.java.java3.m1.assignments.a04listdrinksv2;

public class Runner {
	public static void main(String[] args) {
		Menu menu = new Menu();
		menu.addDrink("LaChouffe", 3.5F, true);
		menu.addDrink("Coca Cola", 2, false);
		menu.addDrink("Spa Sparkling", 2, false);
		menu.addDrink("Spa Still", 2, false);
		menu.addDrink("Coca Cola Light", 2, false);
		menu.addDrink("Coffee", 2.5F, false);
		menu.addDrink("Tea", 2.5F, false);
		menu.addDrink("Pils", 2, true);
		menu.addDrink("Duvel", 3.5F, true);
		menu.addDrink("Orval", 4, true);


		System.out.printf("Our menu contains %d drinks, with a total cost of €%.02f\n", menu.getSize(), menu.getTotalPrice());
		System.out.println(menu);
		System.out.printf("Our most expensive drink is: %s\n", menu.mostExpensiveDrink());
		System.out.printf("Our alcoholic drinks are: %s\n", menu.getAlcoholicDrinks());
		menu.removeMoreExpensiveThan(3);
		System.out.println(menu);
		Drink[] drinks = {new Drink("Bushmills 10yr", 7F, true), new Drink("SpringBank 5yr", 5F, true)};
		menu.addDrinks(drinks);
		System.out.println(menu);
		menu.sort();
		System.out.println(menu);
	}
}
