package be.kdg.java.java3.m1.assignments.a09setgirlsv2;

import java.util.*;

public class Runner {
	public static void main(String[] args) {
		List<Girl> girls = new ArrayList<>(List.of(new Girl("An", 20),new Girl("Bea", 20),
				new Girl("Bea", 25), new Girl("Diana", 25),
				new Girl("Zoë", 18), new Girl("Ekaterina", 18),
				new Girl("Bea", 20)));

		System.out.println(girls);

		TreeSet<Girl> treeGirls = new TreeSet<>(girls);

		System.out.printf("Set of %d girls: %s\n", treeGirls.size(), treeGirls);

		System.out.printf("Last girl: %s\n", treeGirls.last());
		System.out.printf("Girl before Diana: %s\n", treeGirls.floor(new Girl("Diana", 24)));
		System.out.printf("Girl that would be after Dido (21): %s\n", treeGirls.ceiling(new Girl("Dido", 21)));
	}
}
