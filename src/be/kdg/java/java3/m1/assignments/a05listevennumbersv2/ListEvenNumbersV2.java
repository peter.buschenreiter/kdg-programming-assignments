package be.kdg.java.java3.m1.assignments.a05listevennumbersv2;

import java.util.*;

public class ListEvenNumbersV2 {
	public static void main(String[] args) {
		Random random = new Random();
		List<Integer> numberList = new ArrayList<>();

		for (int i = 0; i < 20; i++) {
			numberList.add(random.nextInt(1, 51));
		}

		System.out.println(numberList);

		Integer[] numberArray = numberList.toArray(Integer[]::new);

		for (Integer number : numberArray) {
			System.out.println(number);
		}

		List<Integer> evenNumbers = new ArrayList<>(Arrays.stream(numberArray).toList());

		evenNumbers.removeIf(num -> num % 2 == 1);

		System.out.println(evenNumbers);


		Collections.sort(evenNumbers);
		System.out.print("Ascending: ");
		System.out.println(evenNumbers);

		Collections.reverse(evenNumbers);
		System.out.print("Descending: ");
		System.out.println(evenNumbers);

		Collections.shuffle(evenNumbers);
		System.out.print("Shuffled: ");
		System.out.println(evenNumbers);

		int num = 48;
		int amount = Collections.frequency(evenNumbers, num);
		System.out.printf("Number %d appeared %d times.\n", num, amount);

	}
}
