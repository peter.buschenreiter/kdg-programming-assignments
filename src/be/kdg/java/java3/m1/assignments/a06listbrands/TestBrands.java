package be.kdg.java.java3.m1.assignments.a06listbrands;

public class TestBrands {
	public static void main(String[] args) {
		Brands brands = new Brands();
		System.out.println(brands);

		brands.alphabetic();
		System.out.println(brands);
		brands.alphabeticDescending();
		System.out.println(brands);
	}
}
