package be.kdg.java.java3.m1.assignments.a06listbrands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Brands {
	private List<String> brands = new ArrayList<>(Arrays.stream(Data.brands).toList());

	public void alphabetic() {
		Collections.sort(brands);
	}

	public void alphabeticDescending() {
		Collections.sort(brands);
		Collections.reverse(brands);
	}

	@Override
	public String toString() {
		return brands.toString();
	}
}
