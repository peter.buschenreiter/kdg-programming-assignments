package be.kdg.java.java3.m1.assignments.a06listbrands;

public class Data {
	public final static String[] brands = {
			"BMW", "Audi", "VW", "Ford", "Opel",
			"Renault", "Peugeot", "Citroen", "Mercedes", "Fiat"
	};
}
