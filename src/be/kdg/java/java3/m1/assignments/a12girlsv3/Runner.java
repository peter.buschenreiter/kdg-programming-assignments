package be.kdg.java.java3.m1.assignments.a12girlsv3;

import java.util.*;

public class Runner {
	public static void main(String[] args) {
		List<Girl> girls = new ArrayList<>(List.of(new Girl("An", 20),new Girl("Bea", 20),
				new Girl("Bea", 25), new Girl("Diana", 25),
				new Girl("Zoë", 18), new Girl("Ekaterina", 18),
				new Girl("Bea", 20)));

		Map<String, Girl> girlMap = new HashMap<>();

		for (Girl girl : girls) {
			girlMap.put(girl.getName(), girl);
		}
		System.out.printf("Map of %d girls: %s\n", girlMap.size(), girlMap);

		Map<String, Girl> girlTree = new TreeMap<>();

		for (Girl girl : girls) {
			girlTree.put(girl.getName(), girl);
		}
		System.out.printf("Map of %d girls: %s\n", girlTree.size(), girlTree);

	}
}
