package be.kdg.java.java3.m1.assignments.a12girlsv3;

import java.util.Objects;

public class Girl implements Comparable<Girl>{
	private String name;
	private int age;

	public Girl(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name + " (" + age + ")";
	}

	//	Equals and hash based on age
//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//		Girl girl = (Girl) o;
//		return getAge() == girl.getAge();
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(getAge());
//	}

	//	Equals and hash based on name
//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//		Girl girl = (Girl) o;
//		return Objects.equals(getName(), girl.getName());
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(getName());
//	}

	//	Equals and hash based on name and age
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Girl girl = (Girl) o;
		return getAge() == girl.getAge() && Objects.equals(getName(), girl.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getAge());
	}

	@Override
	public int compareTo(Girl o) {
		if (String.CASE_INSENSITIVE_ORDER.compare(this.getName(), o.getName()) == 0) {
			return Integer.compare(this.getAge(), o.getAge());
		}
		return String.CASE_INSENSITIVE_ORDER.compare(this.getName(), o.getName());
	}
}
