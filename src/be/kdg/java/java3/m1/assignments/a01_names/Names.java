package be.kdg.java.java3.m1.assignments.a01_names;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Names {
	public static void main(String[] args) {
		String[] names = {"Albert", "Henry", "Josephine", "Annabelle", "Ashraf"};

		List<String> namesList = new ArrayList<>(Arrays.stream(names).toList());
		System.out.println(namesList.get(0));
		System.out.println(namesList.get(namesList.size() - 1));

		for (String name : namesList) {
			System.out.println(name);
		}

		String nameGeorgie = "Georgie";

		System.out.printf("%s is%s in the list of names!\n", nameGeorgie, namesList.contains(nameGeorgie) ? "" : " not");

		namesList.removeIf(name -> name.charAt(0) == 'A');

		System.out.println(namesList);

	}
}
