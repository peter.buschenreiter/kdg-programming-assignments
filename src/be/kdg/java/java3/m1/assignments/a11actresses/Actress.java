package be.kdg.java.java3.m1.assignments.a11actresses;

import java.util.Objects;

public class Actress implements Comparable<Actress> {
	private String name;
	private int birthYear;

	public Actress(String name, int birthYear) {
		this.name = name;
		this.birthYear = birthYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	@Override
	public String toString() {
		return name + " (" + birthYear + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Actress actress = (Actress) o;
		return getBirthYear() == actress.getBirthYear() && Objects.equals(getName(), actress.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getBirthYear());
	}

	@Override
	public int compareTo(Actress o) {
		return Integer.compare(this.birthYear, o.birthYear);
	}
}
