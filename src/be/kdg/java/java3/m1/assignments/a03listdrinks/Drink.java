package be.kdg.java.java3.m1.assignments.a03listdrinks;

import java.util.Objects;

public class Drink {
	private String name;
	private float price;
	private boolean isAlcoholic;

	public Drink(String name, float price, boolean isAlcoholic) {
		this.name = name;
		this.price = price;
		this.isAlcoholic = isAlcoholic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isAlcoholic() {
		return isAlcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		isAlcoholic = alcoholic;
	}

	@Override
	public String toString() {
		return String.format("%s €%.02f", name, price);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Drink drink = (Drink) o;
		return Float.compare(drink.getPrice(), getPrice()) == 0 && isAlcoholic() == drink.isAlcoholic() && Objects.equals(getName(), drink.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getPrice(), isAlcoholic());
	}

}
