package be.kdg.java.java3.m1.assignments.a03listdrinks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Menu {
	private final List<Drink> drinks = new ArrayList<>();

	public void addDrink(String name, float price, boolean isAlcohol) {
		drinks.add(new Drink(name, price, isAlcohol));
	}

	public void addDrinks(Drink[] newDrinks) {
		drinks.addAll(Arrays.asList(newDrinks));
	}

	public int getSize() {
		return drinks.size();
	}

	public float getTotalPrice() {
		float sum = 0;
		for (Drink drink : drinks) {
			sum += drink.getPrice();
		}
		return sum;
	}

	public Drink mostExpensiveDrink() {
		Drink mostExpensiveDrink = drinks.get(0);
		float mostExpensive = drinks.get(0).getPrice();

		for (Drink drink : drinks) {
			if (drink.getPrice() > mostExpensive) {
				mostExpensiveDrink = drink;
			}
		}
		return mostExpensiveDrink;
	}

	public List<Drink> getAlcoholicDrinks() {
		return drinks.stream().filter(Drink::isAlcoholic).toList();
	}

	public void removeMoreExpensiveThan(float amount) {
		drinks.removeIf(drink -> drink.getPrice() > amount);
	}



	@Override
	public String toString() {
		return "Menu: [" +
				"drinks=" + drinks +
				"]";
	}
}
