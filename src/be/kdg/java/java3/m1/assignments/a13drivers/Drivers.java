package be.kdg.java.java3.m1.assignments.a13drivers;

import java.util.*;

public class Drivers {
	Map<String, Driver> driverByName = new HashMap<>();
	Map<Integer, Driver> driverByYear = new HashMap<>();
	LinkedList<Driver> drivers = new LinkedList<>();

	public Drivers(String[][] d, Integer[][] y) {
		for (int i = 0; i < d.length; i++) {
			ArrayList<Integer> years = new ArrayList<>(List.of(y[i]));
			Driver driver = new Driver(d[i][1], d[i][0], years);
			driverByName.put(driver.getName(), driver);

			for (int j = 0; j < y[i].length; j++) {
				driverByYear.put(y[i][j], driver);
			}
		}
		drivers.addAll(driverByName.values());
	}

	public void getDriverByName(String name) {
		Driver driver = driverByName.get(name);
		if (driver == null) {
			System.out.printf("%s: not listed", name);
		} else {
			System.out.println(driver);
		}
	}

	public void getDriverByYear(int year) {
		Driver driver = driverByYear.get(year);
		System.out.printf("%d champion: ", year);
		if (driver == null) {
			System.out.print("not listed\n");
		} else {
			System.out.println(driver.getName());
		}
	}

	public Driver[] getDrivers() {
			return driverByName.values().toArray(Driver[]::new);
	}

	public void sort() {
		Collections.sort(drivers);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Driver driver : drivers) {
			sb.append(driver).append("\n");
		}
		return sb.toString();
	}
}
