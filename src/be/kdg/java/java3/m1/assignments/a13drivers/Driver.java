package be.kdg.java.java3.m1.assignments.a13drivers;

import java.util.ArrayList;

public class Driver implements Comparable<Driver> {
	private final String name;
	private final String nationality;
	ArrayList<Integer> yearsChampion;

	public Driver(String name, String nationality, ArrayList<Integer> yearsChampion) {
		this.name = name;
		this.nationality = nationality;
		this.yearsChampion = yearsChampion;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("%s (%s) %d times champion %s", name, nationality, yearsChampion.size(), yearsChampion);
	}

	@Override
	public int compareTo(Driver o) {
		if (this.yearsChampion.size() == o.yearsChampion.size()) {
			return Integer.compare(this.yearsChampion.get(0), o.yearsChampion.get(0));
		}
		return Integer.compare(o.yearsChampion.size(), this.yearsChampion.size());
	}
}
