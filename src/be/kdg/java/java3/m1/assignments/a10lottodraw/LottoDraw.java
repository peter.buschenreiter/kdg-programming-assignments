package be.kdg.java.java3.m1.assignments.a10lottodraw;

import java.util.Random;
import java.util.TreeSet;

public class LottoDraw {
	public static void main(String[] args) {
		TreeSet<Integer> draw = new TreeSet<>();
		Random random = new Random();

		while (draw.size() < 6) {
			draw.add(random.nextInt(1, 45));
		}
		System.out.print("The lotto numbers for today are: ");
		for (Integer number : draw) {
			System.out.print(number + " ");
		}
	}
}
