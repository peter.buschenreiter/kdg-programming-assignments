package be.kdg.java.java3.m3.assignments.a01onstage;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.paint.Color;



public class Main extends Application {
	@Override
	public void start(Stage stage) throws Exception {
		Label helloLabel = new Label("Hello JavaFX!");
		BorderPane root = new BorderPane(helloLabel);
		Scene scene = new Scene(root);
		scene.setCursor(Cursor.CROSSHAIR);

		stage.setMinHeight(500);
		stage.setMinWidth(600);
		stage.setMaxWidth(700);
		stage.setTitle("Fancy Title");
		stage.setScene(scene);
		stage.show();

		Group group = new Group(root);
		Scene secondScene = new Scene(group);
		secondScene.setFill(Color.YELLOW);

		Stage secondStage = new Stage();
		secondStage.setTitle("This is my second amazing Stage");
//		secondStage.setResizable(false);
		secondStage.setMinHeight(200);
		secondStage.setMinWidth(300);
		secondStage.setScene(secondScene);
		secondStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
