package be.kdg.java.java3.m3.assignments.a03reverse.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ReverseView extends GridPane {
	private TextField textField;
	private Button button;

	public ReverseView() {
		initializeNodes();
		layoutNodes();
	}

	private void initializeNodes() {
		textField = new TextField("Default text");
		button = new Button("reverse");
	}

	private void layoutNodes() {
		setVgap(10);
		setHgap(10);
		add(textField, 0, 0);
		setColumnSpan(textField, 2);
		add(button, 1, 1);
		setMargin(button, new Insets(10));
		setPadding(new Insets(10));
	}

	// package-private getters for controls to use in Presenter class

	Button getButton() {
		return button;
	}

	TextField getTextField() {
		return textField;
	}
}
