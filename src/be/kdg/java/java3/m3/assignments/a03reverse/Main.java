package be.kdg.java.java3.m3.assignments.a03reverse;

import be.kdg.java.java3.m3.assignments.a03reverse.model.Reverse;
import be.kdg.java.java3.m3.assignments.a03reverse.view.ReversePresenter;
import be.kdg.java.java3.m3.assignments.a03reverse.view.ReverseView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		Reverse model = new Reverse("Enter text here");
		ReverseView view = new ReverseView();

		new ReversePresenter(model, view);
		primaryStage.setScene(new Scene(view));

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
