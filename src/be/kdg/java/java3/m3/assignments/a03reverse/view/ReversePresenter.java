package be.kdg.java.java3.m3.assignments.a03reverse.view;

import be.kdg.java.java3.m3.assignments.a03reverse.model.Reverse;

public class ReversePresenter {
	private final Reverse model;
	private final ReverseView view;

	public ReversePresenter(Reverse model, ReverseView view) {
		this.model = model;
		this.view = view;

		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getButton().setOnAction(actionEvent -> {
			model.setText(view.getTextField().getText());
			model.reverse();
			updateView();
		});
	}

	private void updateView() {
		view.getTextField().setText(model.getText());
	}
}
