package be.kdg.java.java3.m3.assignments.a03reverse.model;

public class Reverse {
	private String text;

	public Reverse(String text) {
		this.text = text;
	}

	public void reverse() {
		StringBuilder sb = new StringBuilder(text);
		setText(sb.reverse().toString());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
