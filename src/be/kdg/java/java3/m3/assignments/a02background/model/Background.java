package be.kdg.java.java3.m3.assignments.a02background.model;

import java.util.Random;

public class Background {
	private String backgroundColor;
	public static final int MAX_COLOR = 256;

	public Background() {
		backgroundColor = "cornsilk";
	}

	private int randomColor() {
		Random random = new Random();
		return random.nextInt(MAX_COLOR);
	}

	public void setRandomColor() {
		backgroundColor = "rgb(%d, %d, %d)".formatted(randomColor(), randomColor(), randomColor());
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}
}
