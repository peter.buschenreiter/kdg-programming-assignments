package be.kdg.java.java3.m3.assignments.a02background.view;

import be.kdg.java.java3.m3.assignments.a02background.model.Background;

public class BackgroundPresenter {
	private final Background model;
	private final BackgroundView view;

	public BackgroundPresenter(Background model, BackgroundView view) {
		this.model = model;
		this.view = view;

		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getButton().setOnAction(actionEvent -> {
			model.setRandomColor();
			updateView();
		});
	}

	private void updateView() {
		view.setStyle("-fx-background-color: " + model.getBackgroundColor() + ";");
	}
}
