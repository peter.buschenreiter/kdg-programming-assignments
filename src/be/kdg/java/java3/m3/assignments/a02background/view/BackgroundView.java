package be.kdg.java.java3.m3.assignments.a02background.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class BackgroundView extends BorderPane {
	// private Node attributes
	private Button button;

	public BackgroundView() {
		initializeNodes();
		layoutNodes();
	}

	private void initializeNodes() {
		button = new Button("Repaint");
	}

	private void layoutNodes() {
		setBottom(button);
		setAlignment(button, Pos.BOTTOM_RIGHT);
		button.setPadding(new Insets(10));
		setMargin(button, new Insets(10));
		setPadding(new Insets(10));
	}

	// package-private getters for controls to use in Presenter class

	Button getButton() {
		return button;
	}
}
