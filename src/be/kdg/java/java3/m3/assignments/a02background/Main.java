package be.kdg.java.java3.m3.assignments.a02background;

import be.kdg.java.java3.m3.assignments.a02background.model.Background;
import be.kdg.java.java3.m3.assignments.a02background.view.BackgroundPresenter;
import be.kdg.java.java3.m3.assignments.a02background.view.BackgroundView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		Background model = new Background();
		BackgroundView view = new BackgroundView();

		new BackgroundPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.setWidth(400);
		primaryStage.setHeight(250);

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
