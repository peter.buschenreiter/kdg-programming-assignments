package be.kdg.java.java3.m2.assignments.a10javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class HelloJavaFX extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		Label helloLabel = new Label("Hello JavaFX!");
		BorderPane root = new BorderPane(helloLabel);
		Scene scene = new Scene(root);
		stage.setMinHeight(100);
		stage.setMinWidth(100);
		stage.setScene(scene);
		stage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
