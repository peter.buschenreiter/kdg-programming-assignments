package be.kdg.java.java3.m2.assignments.a03playingcards;

import java.util.Locale;
import java.util.Random;

public class PlayingCard {
	public enum Rank {
		TWO(2),
		THREE(3),
		FOUR(4),
		FIVE(5),
		SIX(6),
		SEVEN(7),
		EIGHT(8),
		NINE(9),
		TEN(10),
		JACK(10),
		QUEEN(10),
		KING(10),
		ACE(1);

		private int value;

		Rank(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		@Override
		public String toString() {
			return name().toLowerCase(Locale.ROOT);
		}
	}

	public enum Suit {
		HEARTS,
		SPADES,
		CLUBS,
		DIAMONDS;

		@Override
		public String toString() {
			return name().toLowerCase(Locale.ROOT);
		}
	}

	private static Random random = new Random();
	private Rank rank;
	private Suit suit;

	public PlayingCard(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}

	public int getValue() {
		return getRank().getValue();
	}

	public static PlayingCard generateRandomPlayingCard() {
		return new PlayingCard(Rank.values()[random.nextInt(Rank.values().length)], Suit.values()[random.nextInt(Suit.values().length)]);
	}

	@Override
	public String toString() {
		return String.format("%s of %s", getRank(), getSuit());
	}
}
