package be.kdg.java.java3.m2.assignments.a01daysoftheweek;

public class DaysOfTheWeek {
	public static void main(String[] args) {
		for (DayOfTheWeek day : DayOfTheWeek.values()) {
			System.out.println(day);
		}
	}
}
