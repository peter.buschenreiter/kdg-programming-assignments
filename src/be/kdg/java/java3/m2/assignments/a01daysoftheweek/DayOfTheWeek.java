package be.kdg.java.java3.m2.assignments.a01daysoftheweek;

public enum DayOfTheWeek {
	MONDAY(true),
	TUESDAY(true),
	WEDNESDAY(true),
	THURSDAY(true),
	FRIDAY(true),
	SATURDAY(false),
	SUNDAY(false);

	private final boolean isWeekday;

	private DayOfTheWeek(boolean isWeekday) {
		this.isWeekday = isWeekday;
	}

	public boolean isWeekday() {
		return isWeekday;
	}

	@Override
	public String toString() {
		return String.format("%s (day %d, weekday = %b)", name().charAt(0) + name().substring(1).toLowerCase(), ordinal() + 1, isWeekday());
	}
}
