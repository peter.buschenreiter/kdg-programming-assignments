package be.kdg.java.java3.m2.assignments.a02movie;

public enum Audio {
	DOLBY("Dolby"),
	DOLBY_HD("Dolby HD"),
	DTS_HD("DTS HD"),
	PCM,
	VHS;

	private String printValue = null;

	private Audio(String printValue) {
		this.printValue = printValue;
	}

	private Audio() {}

	public String getPrintValue() {
		return printValue;
	}

	@Override
	public String toString() {
		return String.format("%-10s", getPrintValue() == null ? name() : getPrintValue());
	}
}
