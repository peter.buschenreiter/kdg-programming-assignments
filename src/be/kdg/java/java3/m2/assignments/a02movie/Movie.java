package be.kdg.java.java3.m2.assignments.a02movie;

public class Movie {
	private String title;
	private int year;
	private Format format;
	private Audio audio;

	public Movie(String title, int year, Format format, Audio audio) {
		this.title = title;
		this.year = year;
		this.format = format;
		this.audio = audio;
	}

	@Override
	public String toString() {
		return String.format("%-20s %d %s %s", title, year, format, audio);
	}
}
