package be.kdg.java.java3.m2.assignments.a02movie;

public enum Format {
	BLU_RAY("Blue-ray"),
	DVD,
	VHS;

	private String printValue = null;

	private Format(String printValue) {
		this.printValue = printValue;
	}

	private Format() {}

	public String getPrintValue() {
		return printValue;
	}

	@Override
	public String toString() {
		return String.format("%-10s", getPrintValue() == null ? name() : getPrintValue());
	}
}
