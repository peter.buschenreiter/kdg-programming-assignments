package be.kdg.java.java3.m2.assignments.a08fibonacci;

public class FibonacciException extends ArithmeticException {
	public FibonacciException(String s) {
		super(s);
	}
}
