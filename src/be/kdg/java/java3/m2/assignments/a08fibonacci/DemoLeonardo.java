package be.kdg.java.java3.m2.assignments.a08fibonacci;

public class DemoLeonardo {
	public static void main(String[] args) {
		try {
			Fibonacci.fibonacciNumber(-1);
		} catch (FibonacciException e) {
			System.out.println(e.getMessage());
		}

		for (int i = 0; i < 100; i++) {
			double dividend = 0;
			long divisor = 0;
			try {
				dividend = Fibonacci.fibonacciNumber(i + 1);
				divisor = Fibonacci.fibonacciNumber(i);
			} catch (FibonacciException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
			System.out.printf("f(%d) / f(%d) = %.15f%n", i + 1, i, dividend / divisor);
		}
	}
}
