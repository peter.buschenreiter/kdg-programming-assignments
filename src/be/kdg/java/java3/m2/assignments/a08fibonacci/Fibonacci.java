package be.kdg.java.java3.m2.assignments.a08fibonacci;

public class Fibonacci {
	private static final long MAX = 91;

	public static long fibonacciNumber(int n) {
		long first = 0;
		long second = 1;
		long number = 0;

		if (n < 0) {
			throw new FibonacciException("Negative values are not allowed!\n");
		} else if (n >= MAX) {
			throw new FibonacciException("The maximum value for type 'long' was exceeded!");
		}

		for (int i = 0; i < n; i++) {
			number = first + second;
			first = second;
			second = number;
		}

		return number;
	}
}
