package be.kdg.java.java3.m2.assignments.a09bankaccount;

public class BankAccountValidator {
	private static final int LENGTH = 12;

	public static void validateAccount(String account) throws BankAccountException {
		if (account.length() != LENGTH) {
			throw new BankAccountException("BankAccount must have %d digits".formatted(LENGTH));
		}
		try {
			long accountNumber = Long.parseLong(account);
		} catch (NumberFormatException e) {
			throw new BankAccountException("String must be numeric");
		}
		if (!isValidNumber(account)) {
			throw new BankAccountException("Wrong account number");
		}
	}

	private static boolean isValidNumber(String account) {
		long accountNumber = Long.parseLong(account.substring(0, 10));
		int controlNumber = Integer.parseInt(account.substring(10));
		long remainder = accountNumber % 97;
		if (remainder == 0) {
			return controlNumber == 97;
		} else {
			return controlNumber == remainder;
		}
	}
}
