package be.kdg.java.java3.m2.assignments.a09bankaccount;

public class BankAccount {
	private final String account;

	public BankAccount(String account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return account.substring(0, 3) + "-" + account.substring(3, 10) + "-" + account.substring(10);
	}
}
