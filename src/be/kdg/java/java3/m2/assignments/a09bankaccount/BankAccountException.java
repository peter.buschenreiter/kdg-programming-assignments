package be.kdg.java.java3.m2.assignments.a09bankaccount;

public class BankAccountException extends Exception{
	public BankAccountException() {
	}

	public BankAccountException(String message) {
		super(message);
	}

	public BankAccountException(String message, Throwable cause) {
		super(message, cause);
	}

	public BankAccountException(Throwable cause) {
		super(cause);
	}
}
