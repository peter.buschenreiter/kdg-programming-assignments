package be.kdg.java.java3.m2.assignments.a05moviesandactors;

public class Movie {
	public class Actor {
		private String name;

		public Actor(String name) {
			this.name = name;
		}

		public String getName() {
			return name == null ? "unknown" : name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return getName();
		}
	}

	private String title;
	private Integer year;
	private Actor leadingRole;
	private Actor supportingRole;

	public Movie(String title, Integer year, String leadingRoleName, String supportingRoleName) {
		this.title = title;
		this.year = year;
		leadingRole = new Actor(leadingRoleName);
		supportingRole = new Actor(supportingRoleName);
	}

	public Movie(String title, Integer year) {
		this(title, year, null, null);
	}

	public Movie(String title) {
		this(title, null, null, null);
	}

	public String getTitle() {
		return title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Actor getLeadingRole() {
		return leadingRole;
	}

	public void setLeadingRole(Actor leadingRole) {
		this.leadingRole = leadingRole;
	}

	public Actor getSupportingRole() {
		return supportingRole;
	}

	public void setSupportingRole(Actor supportingRole) {
		this.supportingRole = supportingRole;
	}

	@Override
	public String toString() {
		return String.format("Title: %-40s; Year: %-7s; Cast: %s, %s", title, year == null ? "unknown" : year, leadingRole.getName(), supportingRole.getName());
	}
}
