package be.kdg.java.java3.m2.assignments.a06rockpaperscissors;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	public enum Sign {
		ROCK,
		PAPER,
		SCISSORS;
	}

	public static void main(String[] args) {
		String input;
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		String signCPU;


		while (true) {
			System.out.print("Please enter 'rock', 'paper', or 'scissors' (or 'stop'): ");
			input = scanner.nextLine();

			if ("stop".equals(input)) break;

			signCPU = Sign.values()[random.nextInt(Sign.values().length)].toString();

			switch (input) {
				case "rock", "paper", "scissors" -> {
					System.out.printf("Computer chose %s.\n", signCPU);
					int choice = Sign.valueOf(input.toUpperCase(Locale.ROOT)).ordinal() + 1;
					int comp = Sign.valueOf(signCPU.toUpperCase(Locale.ROOT)).ordinal() + 1;
					if (choice == comp) System.out.println("It's a tie!");
					else if ((choice + 3 - 2) % 3 == comp - 1) System.out.println("You won, congratulations!");
					else System.out.println("Computer won!");
					System.out.println();
				}
				default -> throw new InputMismatchException("Input is invalid.");


			}

		}
	}
}
