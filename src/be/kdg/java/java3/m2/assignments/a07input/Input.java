package be.kdg.java.java3.m2.assignments.a07input;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Input {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean isValidNumber = false;
		int number = 0;

		while (!isValidNumber) {
			try {
				System.out.print("Please enter a number from 1 to 10: ");
				number = scanner.nextInt();
				if (number < 1 || number > 10) {
					throw new InputMismatchException(String.format("%d is not a number from 1 to 10.", number));
				}
				isValidNumber = true;
			} catch (InputMismatchException e) {
				scanner.nextLine();
				System.out.println(e.getMessage());
			}
		}
		System.out.printf("%d is a valid number!", number);
	}
}
